﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Core
{
    /// <summary>
    /// The information from this class will always be the same. 
    /// TODO: eventually break this out into a database and make this into a separate program
    /// TODO: should I really make a static service (aka an inevitable utilities). Could pull this into Hand class
    /// </summary>
    public static class CalculatorService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberOfCards">Number of cards being looked through (EX 6 for dealt cards</param>
        /// <param name="numberOfCardsInSet">Number of cards group together to be defined as unique (EX 4 for cards kept</param>
        /// <returns></returns>
        public static List<bool[]> GetAllPossibleCombinations(int numberOfCards, int numberOfCardsInSet)
        {
            //List that holds all possible combinations that are valid for the request
            List<bool[]> allValidCombinations = new List<bool[]>();
            //List that holds all possible combinations the are possible
            List<bool[]> allCombinations = new List<bool[]>();

            //Get all possible combinations
            allCombinations = GetAllCombinations(numberOfCards);

            foreach (bool[] combination in allCombinations)
            {
                int count = 0;
                for (int i = 0; i < combination.Length; i++)
                {
                    if (combination[i])
                    {
                        count++;
                    }
                }

                //If the count = NUMBER_OF_CARDS_IN_CRIBBAGE_HAND then this is a valid combination for a cribbage hand
                if (numberOfCardsInSet == count)
                {
                    allValidCombinations.Add(combination);
                }
            }

            return allValidCombinations;
        }

        /// <summary>
        /// Uses a binary counter to obtain all possible permutations. For example, getting all possible combinations of 4 cards to keep 
        /// out of the original 6 that are dealt to you.
        /// </summary>
        /// <param name="numberOfCards">Number of cards being looked through (EX 6 for dealt cards)</param>
        /// <returns></returns>
        public static List<bool[]> GetAllCombinations(int numberOfCards)
        {
            // The possible spots
            bool[] validSpots = new bool[numberOfCards];
            bool allCombinationsChecked = true;

            // Initialize this to Zero (all false)
            for (int i = 0; i < numberOfCards; i++)
            {
                validSpots[i] = false;
            }

            // List that holds all possible combinations
            List<bool[]> allCombinations = new List<bool[]>();
            
            // Add the first index (all false) to the list of possible combinations
            bool[] tempArray = new bool[validSpots.Length];
            Array.Copy(validSpots, tempArray, validSpots.Length);
            allCombinations.Add(tempArray);

            // Get all possible combinations
            // Continue getting the information if not all combinations have been checked
            do
            {
                bool carryOver = false;
                for (int i = 0; i < validSpots.Length; i++)
                {
                    //Always add a 'true' to the first index
                    if (i == 0)
                    {
                        if (validSpots[i])
                        {
                            carryOver = true;
                            validSpots[i] = false;
                        }
                        else
                        {
                            validSpots[i] = true;
                            break;
                        }
                    }
                    else
                    {
                        if (validSpots[i] && carryOver)
                        {
                            validSpots[i] = false;
                        }
                        else if (!validSpots[i] && carryOver)
                        {
                            validSpots[i] = true;
                            carryOver = false;
                            break;
                        }
                        else
                        {
                            //There was no carry over, so the spot maintains its value
                            break;
                        }
                    }
                }

                tempArray = new bool[validSpots.Length];
                Array.Copy(validSpots, tempArray, validSpots.Length);
                allCombinations.Add(tempArray);

                // Check if all spots are true, this means that all possible combinations have been found
                // TODO: KEF could also do a basic 2^numberOfCards length check
                allCombinationsChecked = true;
                for (int i = 0; i < validSpots.Length; i++)
                {
                    if (!validSpots[i])
                    {
                        allCombinationsChecked = false;
                        break;
                    }
                }
            } while (!allCombinationsChecked);

            return allCombinations;
        }
    }
}
