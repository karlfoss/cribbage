﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// Constants that should apply to all implementations
    /// </summary>
    public class GeneralConstants
    {
        // Game Rules
        public const int NUMBER_OF_CARDS_DEALT = 6;
        public const int NUMBER_OF_CARDS_KEPT = 4;
        public const int NUMBER_OF_CARDS_NOT_IN_HAND = 46;
        public const int NUMBER_OF_CARDS_TO_DISCARD_FOR_CRIB = 2;
        public const int NUMBER_OF_CARDS_TO_PLAY_FOR_PEGGING = 1;

        // Combinations that lead to points
        public const int VALID_HAND_SIZE = 5;
        public const int NUMBER_OF_CARDS_IN_PAIR = 2;
        public const int NUMBER_OF_CARDS_IN_RUN_OF_FIVE = 5;
        public const int NUMBER_OF_CARDS_IN_RUN_OF_FOUR = 4;
        public const int NUMBER_OF_CARDS_IN_RUN_OF_THREE = 3;
        public const int FIRST_CARD_IN_PAIR = 0;
        public const int SECOND_CARD_IN_PAIR = 1;
        public const int POINTS_NEEDED_FOR_FIFTEEN = 15;

        // Points from certain combinations
        public const int POINTS_FROM_PAIR = 2;
        public const int POINTS_FROM_KNOBS = 1;
        public const int POINTS_FROM_FLUSH = 4;
        public const int POINTS_FROM_FLUSH_WITH_MATCHING_CUT_CARD = 5;
        public const int POINTS_FROM_FIFTEEN = 2;
        public const int POINTS_FROM_RUN_OF_FIVE = 5;
        public const int POINTS_FROM_RUN_OF_FOUR = 4;
        public const int POINTS_FROM_RUN_OF_THREE = 3;

        // Constants for Cards
        public const int NUMBER_OF_SUITS = 4;
        public const int NUMBER_OF_CARDS_PER_SUIT = 13;
        public const int NUMBER_OF_CARDS_PER_DECK = 52;
        public const int NUMBER_OF_SHUFFLES = 1000;
        // Ten, Jack, Queen, and King all worth 10 points
        public const int LOWEST_CARD_TYPE_WORTH_TEN_POINTS = 9;
        public const int TEN_POINT_CARD = 10;

        //Constants for pegging round
        public const int HIGHEST_COUNT_IN_ROUNDS = 31;
        public const int COUNT_OF_31 = 31;
        public const int COUNT_OF_15 = 15;
        public const int POINTS_FROM_THIRTY_ONE = 2;
        public const int POINTS_FROM_LAST_CARD = 1;
        public const int POINTS_FROM_GO = 1;

        //The dealer gets 2 points if a jack is cut
        public const int POINTS_FROM_CUT_JACK = 2;

        public const int POINTS_NEEDED_TO_WIN = 121;
    }
}
