﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Core
{
    public class BasePlayer
    {
        // The players current hand
        public BaseHand Hand
        {
            get; set;
        }

        // The player's crib (if they are the dealer)
        // IE different points for flushes.
        public BaseHand Crib
        {
            get; set;
        }
        
        // The total points the player has, 121 needed to win
        // Consider making at total points object since we have 2 objects in this class that involve points
        public int TotalPoints
        {
            get; set;
        }
        
        // True if the player is the dealer for the round
        public bool IsDealer
        {
            get; set;
        }

        // Constructor for base player
        public BasePlayer()
        {
            Hand = new BaseHand(false);
            Crib = new BaseHand(true);
            TotalPoints = 0;
        }
    }
}
