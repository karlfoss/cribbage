﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Core
{
    public class BaseCard
    {
        // Unique value of the card (IE 0 - 51)
        public int Value
        {
            get; set;
        }

        // The type of card (IE King, Queen, etc)
        public int Type
        {
            get; set;
        }

        // The suit of the card (Spades, Hearts, Diamonds, Clubs)
        public int Suit
        {
            get; set;
        }

        // The number of points the card is worth
        public int Points
        {
            get; set;
        }

        // True if this is the cut from the deck
        public bool IsCutCard
        {
            get; set;
        }

        /// <summary>
        /// True if this card was selected by a player
        /// Will be true if selected for crib or pegging
        /// </summary>
        public bool IsSelected
        {
            get; set;
        }

        // Card Types
        public enum Types
        {
            Ace,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King
        }

        // Possible Card Suits
        public enum Suits
        {
            Club,
            Diamond,
            Heart,
            Spade
        }

        public BaseCard()
        {
            Value = -1;
            Suit = -1;
            Type = -1;
            Points = -1;
        }

        public override string ToString()
        {
            return String.Format("Value: " + Value + " Points: " + Points + " Suit: " + Suit + " Type: " + Type);
        }
    }
}
