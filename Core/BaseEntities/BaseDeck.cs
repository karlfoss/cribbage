﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class BaseDeck
    {
        #region Constants

        private const int NUMBER_OF_SUITS = 4;
        private const int NUMBER_OF_CARDS_PER_SUIT = 13;
        private const int NUMBER_OF_CARDS_PER_DECK = 52;
        private const int NUMBER_OF_SHUFFLES = 1000;
        // Ten, Jack, Queen, and King all worth 10 points
        private const int LOWEST_CARD_TYPE_WORTH_TEN_POINTS = 9;
        private const int TEN_POINT_CARD = 10;

        #endregion

        #region Properties

        //All of the cards in the deck
        public Stack<BaseCard> DeckOfCards
        {
            get; set;
        }

        //Card cut from the deck after putting cards in the crib
        public BaseCard CutCard
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /*
         * Constructor for deck with no visual elements, 
         * Loads all 52 standard cards in a deck, and then shuffles them
         */
        public BaseDeck()
        {
            DeckOfCards = new Stack<BaseCard>();
            NewDeck();
        }

        /*
         * Shuffles the cards by switching 2 cards at 'random' multiple times
         */
        public void Shuffle()
        {
            Random rn = new Random();
            int card1;
            int card2;
            List<BaseCard> shuffledDeckOfCards = DeckOfCards.ToList();

            for (int i = 0; i < NUMBER_OF_SHUFFLES; i++)
            {
                card1 = rn.Next(0, NUMBER_OF_CARDS_PER_DECK);
                card2 = rn.Next(0, NUMBER_OF_CARDS_PER_DECK);

                BaseCard tempCard = shuffledDeckOfCards[card1];
                shuffledDeckOfCards[card1] = shuffledDeckOfCards[card2];
                shuffledDeckOfCards[card2] = tempCard;
            }

            // Put the cards back into the stack
            DeckOfCards.Clear();
            for (int i = 0; i < NUMBER_OF_CARDS_PER_DECK; i++)
            {
                DeckOfCards.Push(shuffledDeckOfCards[i]);
            }
        }

        /*
         * Returns the top card from the deck (removing it from the deck in the process)
         */
        public BaseCard DrawCard()
        {
            return DeckOfCards.Pop();
        }

        /*
         * Converts the entire deck to a string
         */
        public override string ToString()
        {
            StringBuilder allCardsInDeck = new StringBuilder();
            if (DeckOfCards != null && DeckOfCards.Count > 0)
            {
                List<BaseCard> printDeckOfCards = DeckOfCards.ToList<BaseCard>();

                for (int i = 0; i < printDeckOfCards.Count; i++)
                {
                    allCardsInDeck.Append(String.Format("Index: {0}  Value: {1}  Type: {2}  Suit: {3}", i, printDeckOfCards[i].Value, printDeckOfCards[i].Type.ToString(), printDeckOfCards[i].Suit.ToString()));
                }
            }
            else
            {
                allCardsInDeck.Append("Deck is null or empty");
            }

            return allCardsInDeck.ToString();
        }

        /*
         * Loads all 52 cards of a standard deck 
         */
        public void NewDeck()
        {
            // Burn the deck before loading a new one
            DeckOfCards.Clear();

            // Go through each suit
            for (int i = 0; i < NUMBER_OF_CARDS_PER_SUIT; i++)
            {
                // Go through all 13 cards per suit
                for (int j = 0; j < NUMBER_OF_SUITS; j++)
                {
                    BaseCard newCard = new BaseCard();

                    newCard.Suit = j;
                    newCard.Type = i;
                    newCard.Value = DeckOfCards.Count;

                    // Ten, Jack, Queen, and King all all worth 10 points
                    if (i > LOWEST_CARD_TYPE_WORTH_TEN_POINTS)
                    {
                        newCard.Points = TEN_POINT_CARD;
                    }
                    else
                    {
                        //j (or CARD Type) is an index that starts at 0, but the lowest point value for a card is 1 (Ace)
                        newCard.Points = i + 1;
                    }

                    // Add the new card to the deck
                    DeckOfCards.Push(newCard);
                }
            }
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
