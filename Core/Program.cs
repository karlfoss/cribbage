﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Program
    {
        //TODO: the program wouldn't start without this. But Core has no entry points. How to resolve?
        public static void Main(String[] args)
        {
            CalculatorService.GetAllPossibleCombinations(6, 4);
        }
    }
}
