﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data;
using System.Drawing;

namespace Game
{
    /// <summary>
    /// The base representation of all games for all common functionality and properties for any game instance
    /// 
    /// Relevant Notes
    /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// https://stackoverflow.com/questions/31781696/define-interface-method-with-different-parameters-in-c-sharp
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/interfaces/
    /// </summary>
    public class BaseGame
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// The deck
        /// </summary>
        public Deck Deck
        {
            get; set;
        }

        /// <summary>
        /// The crib
        /// </summary>
        public Crib Crib
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Default Constructor
        /// </summary>
        public BaseGame()
        {
            Deck = new Deck();
            //Board = new Board();
            Crib = new Crib();
        }

        #endregion

        #region Events

        #endregion

        #region Private Methods

        #endregion
    }
}
