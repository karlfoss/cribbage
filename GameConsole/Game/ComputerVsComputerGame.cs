﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data;

namespace Game
{
    /// <summary>
    /// A standard 2 player game that pits a computer against one of the AI instances. Follows all of the basic rules
    /// of cribbage
    /// 
    /// Relevant Documentation:
    /// https://stackoverflow.com/questions/31781696/define-interface-method-with-different-parameters-in-c-sharp
    /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/interfaces/
    /// GameControl uses State design pattern: http://www.dofactory.com/net/state-design-pattern
    /// Event Guidelines: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/events/
    /// Finite State Machine: https://en.wikipedia.org/wiki/Finite-state_machine
    /// </summary>
    public class ComputerVsComputerGame : BaseGame
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// Both of the players for the 2 player game
        /// NOTE: this is a base class declaration (it is abstract), so Player can not actually be used to initialize
        /// </summary>
        public List<Player> Players
        {
            get; set;
        }

        /// <summary>
        /// The computer player in the 2 player game
        /// NOTE: this is a base class declaration (it is abstract), so Computer can not actually be used to initialize
        /// </summary>
        public Computer ComputerPlayer1
        {
            get; set;
        }

        /// <summary>
        /// The computer player in the 2 player game
        /// NOTE: this is a base class declaration (it is abstract), so Computer can not actually be used to initialize
        /// </summary>
        public Computer ComputerPlayer2
        {
            get; set;
        }

        /// <summary>
        /// Button for moving from game action to game action. These triggers are done by clicks
        /// from the human player
        /// Includes:
        ///     Deal - Deals cards to both players
        ///     Discard - Discard cards to crib
        /// </summary>
        public GameControl GameControl
        {
            get; set;
        }


        /// <summary>
        /// Keeps track of the pegging round and manages counting and cards played per pegging round
        /// </summary>
        public Play Play
        {
            get; set;
        }

        /// <summary>
        /// All cards used in the current round of pegging (resets at Go/31 points)
        /// index 0 is the first card played in the round up to n number of cards
        /// </summary>
        public List<Card> AllPlayedCardsInRound
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public ComputerVsComputerGame(Computer computer1, Computer computer2): base()
        {
            // Set everything that will be needed for the game to run
            ComputerPlayer1 = computer1;
            ComputerPlayer2 = computer2;
            Players = new List<Player>();
            Players.Add(ComputerPlayer1);
            Players.Add(ComputerPlayer2);

            GameControl = new GameControl();
        }

        /// <summary>
        /// This starts the games and initializes all tracked metrics.
        /// TODO: enhance this to either return statistics or store them in a database via an entity framework as an alternative
        /// </summary>
        public void PlayGame(int numberOfGamesToPlay)
        {
            int numberOfGamesPlayed = 0;
            int playerOneWins = 0;
            int playerTwoWins = 0;

            do
            {
                // True at the beginning of the game and false afterwards
                GameControl.IsFirstRound = false;
                // False until a winner is found
                GameControl.WeHaveAWinner = false;

                ComputerPlayer1.TotalPoints = 0;
                ComputerPlayer2.TotalPoints = 0;

                // TODO: add log4net so log levels can be used to determine how much detail is logged out
                // Determine who deals and begin the game-flow
                CutForDeal();

                // Continue playing the game until someone wins
                while (!GameControl.WeHaveAWinner)
                {
                    // Deal another round if no winner is found
                    DealTheCards();

                    // Have the players choose their cards for the crib
                    ChoseCardsForCrib();

                    // Cut for the dealer
                    // Odd logic, but it works because the game continues until we have winner.
                    if (CutCardFromDeck())
                    {
                        GameControl.WeHaveAWinner = true;
                    }
                    else if (PlayAllRounds())
                    {
                        GameControl.WeHaveAWinner = true;
                    }
                    else if (Show())
                    {
                        GameControl.WeHaveAWinner = true;
                    }
                }

                numberOfGamesPlayed++;
                if (ComputerPlayer1.TotalPoints == 121)
                {
                    playerOneWins++;
                }
                else
                {
                    playerTwoWins++;
                }

            } while (numberOfGamesPlayed < numberOfGamesToPlay);

            Console.WriteLine(ComputerPlayer1.PlayerName + " won " + playerOneWins + " out of " + numberOfGamesToPlay + " games");
            Console.WriteLine(ComputerPlayer2.PlayerName + " won " + playerTwoWins + " out of " + numberOfGamesToPlay + " games");

            foreach (Player player in Players)
            {
                Console.WriteLine(player.PlayerName + " scored an average of " + (double) player.PlayerStats.TotalPointsGainedFromHands / player.PlayerStats.NumberOfHands + " points per hand");
                Console.WriteLine(player.PlayerName + " has scored a total of " + player.PlayerStats.TotalPointsGainedFromHands + " points from all time from all of their hands");
                Console.WriteLine(player.PlayerName + " played " + player.PlayerStats.NumberOfHands + " hands");
                Console.WriteLine(player.PlayerName + " scored an average of " + (double) player.PlayerStats.TotalPointsGainedFromCribs / player.PlayerStats.NumberOfCribs + " points per crib");
                Console.WriteLine(player.PlayerName + " has scored a total of " + player.PlayerStats.TotalPointsGainedFromCribs + " points from all  time from all of their cribs");
                Console.WriteLine(player.PlayerName + " played " +  player.PlayerStats.NumberOfCribs + " cribs");
                Console.WriteLine(player.PlayerName + " scored an average of " + (double) player.PlayerStats.TotalPointsGainedFromPlay / player.PlayerStats.NumberOfPlays + " points per play round");
                Console.WriteLine(player.PlayerName + " has scored a total of " + player.PlayerStats.TotalPointsGainedFromPlay + " points from all time from all of their plays");
                Console.WriteLine(player.PlayerName + " played " + player.PlayerStats.NumberOfPlays + " play rounds");
            }

            Console.WriteLine("Press any key to continue.....");
            Console.ReadKey();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Allows the computers to choose 1 of 52 cards to see who will deal first. Low card deals and aces are low.
        /// 
        /// This is only done once per game
        /// </summary>
        private void CutForDeal()
        {
            // Disable the cut for deal button
            GameControl.CurrentGameState = GameControl.GameState.CutForDeal.ToString();

            // Ensure hands are cleared and not shown before cut
            ComputerPlayer1.Hand = new Hand(false);
            ComputerPlayer2.Hand = new Hand(false);

            bool noDealerFound = true;

            // Continue drawing cards until a dealer is chosen
            do
            {
                // Shuffle the deck
                Deck.NewDeck();
                Deck.Shuffle();

                Card computerPlayer1Card = Deck.DrawCard();
                Card computerPlayer2Card = Deck.DrawCard();

                if (computerPlayer1Card.Type < computerPlayer2Card.Type)
                {
                    ComputerPlayer1.IsDealer = true;
                    ComputerPlayer2.IsDealer = false;
                    noDealerFound = false;
                }
                else if(computerPlayer2Card.Type < computerPlayer1Card.Type)
                {
                    ComputerPlayer1.IsDealer = false;
                    ComputerPlayer2.IsDealer = true;
                    noDealerFound = false;
                }
                else
                {
                    //The card types were a draw, cut again
                }

            } while (noDealerFound);
        }

        /// <summary>
        /// Have the dealer give each player 6 cards from the shuffled deck
        /// Switch the dealer each round after the first round
        /// </summary>
        private void DealTheCards()
        {
            GameControl.CurrentGameState = GameControl.GameState.DealCardsToPlayers.ToString();

            // Creates a new Deck and shuffles it
            Deck.ResetForRound();
            // Reset the crib
            Crib = new Crib();

            // Switch the Dealer if it isn't the first round of play (Dealer switches every round)
            if (!GameControl.IsFirstRound)
            {
                ComputerPlayer1.IsDealer = !ComputerPlayer1.IsDealer;
                ComputerPlayer2.IsDealer = !ComputerPlayer2.IsDealer;
                // No longer the first round going forward
                GameControl.IsFirstRound = false;
            }

            // Resets the player's hand
            ComputerPlayer1.ResetForRound();
            ComputerPlayer2.ResetForRound();

            // Deal six the cards to the players for a 2 player game
            for (int i = 0; i < GeneralConstants.NUMBER_OF_CARDS_DEALT; i++)
            {
                foreach (Player player in Players)
                {
                    player.Hand.CardsInHand.Add(Deck.DrawCard());
                }
            }
        }

        /// <summary>
        /// Have the players choose two cards for the crib
        /// The crib contains 4 cards used by the dealer after both players count their hand score
        /// </summary>
        private void ChoseCardsForCrib()
        {
            GameControl.CurrentGameState = GameControl.GameState.ChooseCardsForCrib.ToString();

            // TODO: consider if it is better to move cards to crib via caller or callee (again no right or wrong answer)
            foreach (Computer computerPlayer in Players)
            {
                // Have players choose two cards for the crib in a 2 player game
                List<Card> cardsSelectedForCrib = computerPlayer.GetCardsToPutInCrib();
                // Add the cards to the crib NOTE: cards where isSelected is true are the ones chosen for the crib
                Crib.CribHand.CardsInHand.AddRange(cardsSelectedForCrib);
                // Remove cards selected for discard to the crib
                computerPlayer.Hand.CardsInHand.RemoveAll(a => a.IsSelected == true);
            }
        }

        /// <summary>
        /// Traditionally, the non-dealer cuts the deck for the face up card. This face up card is used during the scoring
        /// rounds after pegging
        /// </summary>
        private bool CutCardFromDeck()
        {
            GameControl.CurrentGameState = GameControl.GameState.CutCardFromDeck.ToString();

            // Add cards to the current dealer's crib
            Player dealer = Players.Single(a => a.IsDealer);

            // Cut the card for the deck
            Deck.CutCardForDeck();

            // Give the dealer 2 points if a jack comes up on the cut (called 'his heels')
            if (Deck.CutCard.Type == (int)Card.Types.Jack)
            {
                dealer.TotalPoints += 2;
                Console.WriteLine(dealer.PlayerName + " scored " + GeneralConstants.POINTS_FROM_CUT_JACK + " points for his heels (aka the jack being cut)");
                
                // Check if the dealer won
                if (dealer.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN)
                {
                    // The player won, so they can't score above 121
                    dealer.TotalPoints = GeneralConstants.POINTS_NEEDED_TO_WIN;

                    Console.WriteLine(dealer.PlayerName + " has a total score of " + dealer.TotalPoints);

                    // TODO: log metrics, store data, etc
                    Console.WriteLine(dealer.PlayerName + " has won the game!");

                    // Player won, end game
                    return true;
                }

                Console.WriteLine(dealer.PlayerName + " has a total score of " + dealer.TotalPoints);
            }

            // Player didn't win, the hunt continues
            return false;
        }

        /// <summary>
        /// Play a single move (for the current player) in the pegging round
        /// </summary>
        /// <returns>True if somebody wins during the play, false if the play is over and nobody won</returns>
        private bool PlayAllRounds()
        {
            
            bool isPlayOver = false;
            // Initialize for the playing round
            InitializePegging();
            ComputerPlayer1.PlayerStats.NumberOfPlays++;
            ComputerPlayer2.PlayerStats.NumberOfPlays++;

            // Continue until all cards in each players hands are played
            do
            {

                // Get the player whose turn it is
                // The player who will play the next card
                Computer currentPlayer = (Computer)Players.Single(a => a.IsTurn);
                // The other player who won't play the next card
                Computer nextPlayer = (Computer)Players.Single(a => !a.IsTurn);

                //######WARNING BUG is at this line. This calculator fails because a new play object is created after a go or 31 is reached. 
                int numberOfOpponentMovesLeft = nextPlayer.Hand.CardsInHand.Count(a => a.IsPlayed == false);
                // Get the card to play
                Card cardToPlay = currentPlayer.GetCardForPlay(Play, numberOfOpponentMovesLeft);
                Play.CardsPlayedInRound.Add(cardToPlay);

                // Count any potential points and check if the player won
                if (CountPointsFromPegOfPlayerAndCheckForWin(currentPlayer))
                {
                    currentPlayer.PlayerStats.TotalPointsGainedFromPlay += Play.PointsFromPreviousPlay;
                    // Game is over as the player won during pegging
                    return true;
                }
                currentPlayer.PlayerStats.TotalPointsGainedFromPlay += Play.PointsFromPreviousPlay;

                bool currentPlayerCanPlayThisRound = currentPlayer.Hand.CardsInHand.Where(a => !a.IsPlayed && a.Points + Play.Count <= GeneralConstants.COUNT_OF_31).ToList().Count > 0;
                bool nextPlayerCanPlayThisRound = nextPlayer.Hand.CardsInHand.Where(a => !a.IsPlayed && a.Points + Play.Count <= GeneralConstants.COUNT_OF_31).ToList().Count > 0;
                bool currentPlayerCanPlayNextRound = currentPlayer.Hand.CardsInHand.Where(a => !a.IsPlayed).ToList().Count > 0;
                bool nextPlayerCanPlayNextRound = nextPlayer.Hand.CardsInHand.Where(a => !a.IsPlayed).ToList().Count > 0;
                bool isCount31 = (GeneralConstants.COUNT_OF_31 == Play.Count);

                // TODO: consider doing a do/while here instead (this would allow moving the moving of the initializing pegging into this method, and reduce recursion)
                if (isCount31 && nextPlayerCanPlayNextRound)
                {
                    // Change turns if the next player can play
                    currentPlayer.IsTurn = false;
                    nextPlayer.IsTurn = true;
                    ResetPeggingRound();
                }
                else if (isCount31 && !nextPlayerCanPlayNextRound && currentPlayerCanPlayNextRound)
                {
                    // Don't change turns since the next player can't play
                    // 2 Points were awarded for reaching 31
                    ResetPeggingRound();
                }
                else if (isCount31 && !nextPlayerCanPlayNextRound && !currentPlayerCanPlayNextRound)
                {
                    // Neither player has any cards left, thus this is the end of the pegging for this deal and move to the card counting phase
                    // 2 Points were awarded for reaching 31
                    // End of the play
                    isPlayOver = true;
                }
                else if (!isCount31 && nextPlayerCanPlayThisRound)
                {
                    // Change turns if the next player can play
                    currentPlayer.IsTurn = false;
                    nextPlayer.IsTurn = true;
                }
                else if (!isCount31 && !nextPlayerCanPlayThisRound && currentPlayerCanPlayThisRound)
                {
                    // Don't change turns since the next player can't play
                }
                else if (!isCount31 && !nextPlayerCanPlayThisRound && !currentPlayerCanPlayThisRound && nextPlayerCanPlayNextRound)
                {
                    // Current player gets a 'Go' and the turns change since the next player still has cards left
                    // Check if the point from the Go was enough to win
                    if (CountPointsFromGoAndCheckForWin(currentPlayer))
                    {
                        currentPlayer.PlayerStats.TotalPointsGainedFromPlay++;
                        // Player won, end of game
                        return true;
                    }
                    currentPlayer.PlayerStats.TotalPointsGainedFromPlay++;

                    currentPlayer.IsTurn = false;
                    nextPlayer.IsTurn = true;
                    ResetPeggingRound();
                }
                else if (!isCount31 && !nextPlayerCanPlayThisRound && !currentPlayerCanPlayThisRound && !nextPlayerCanPlayNextRound && currentPlayerCanPlayNextRound)
                {
                    // Current player gets a 'Go', but the turns don't change since the next player has no cards left

                    // Check if the point from the Go was enough to win
                    if (CountPointsFromGoAndCheckForWin(currentPlayer))
                    {
                        currentPlayer.PlayerStats.TotalPointsGainedFromPlay++;
                        // Player won, end of game
                        return true;
                    }
                    currentPlayer.PlayerStats.TotalPointsGainedFromPlay++;

                    ResetPeggingRound();
                }
                else if (!isCount31 && !nextPlayerCanPlayThisRound && !currentPlayerCanPlayThisRound && !nextPlayerCanPlayNextRound && !currentPlayerCanPlayNextRound)
                {
                    // Neither player can go, so take the point for the last card and move to the card counting phase

                    // Check if the point from the Go was enough to win
                    if (CountPointsFromLastCardOfPlayerAndCheckForWin(currentPlayer))
                    {
                        currentPlayer.PlayerStats.TotalPointsGainedFromPlay++;
                        // Player won, end of game
                        return true;
                    }
                    currentPlayer.PlayerStats.TotalPointsGainedFromPlay++;

                    // End of the play
                    isPlayOver = true;
                }
                else
                {
                    throw new InvalidOperationException("Unknown pegging state, where pegging hasn't finished, and no conditions were met");
                }
            } while (!isPlayOver);

            return false;
        }

        
        /// <summary>
        /// Reveal the hands and count all of the points from each hand and the crib.
        /// 1. Count non dealer first
        /// 2. Count dealer hand second
        /// 3. Count the crib of the dealer
        /// </summary>
        /// <returns>true if a player wins, or false if the game is not over yet</returns>
        private bool Show()
        {
            GameControl.CurrentGameState = GameControl.GameState.CountNonDealersHand.ToString();
            // Get the non dealer to count the points from their hand
            Player nonDealer = Players.Single(a => !a.IsDealer);
            if (CountPointsInHandOfPlayerAndCheckForWin(nonDealer, false))
            {
                return true;
            }

            GameControl.CurrentGameState = GameControl.GameState.CountDealersHand.ToString();
            // Get the dealer to count their points
            Player dealer = Players.Single(a => a.IsDealer);
            // Count points in the dealer's hand and check for win
            if (CountPointsInHandOfPlayerAndCheckForWin(dealer, false))
            {
                return true;
            }

            GameControl.CurrentGameState = GameControl.GameState.CountCrib.ToString();
            // Move the cards from the crib to the players hand for counting
            dealer.Hand = Crib.CribHand;

            // Counts the points in the crib and checks for win
            return CountPointsInHandOfPlayerAndCheckForWin(dealer, true);
        }


        /// <summary>
        /// Count the cards in the players hand
        /// Returns true if the player won with their most recent hand
        /// </summary>
        /// <param name="player"></param>
        /// <param name="countCrib"></param>
        private bool CountPointsInHandOfPlayerAndCheckForWin(Player player, bool isCountingCrib)
        {
            int pointsInHand = 0;

            if (isCountingCrib)
            {
                // Crib is counted last, so it is safe to replace a players hand with the crib
                player.Hand.CardsInHand = Crib.CribHand.CardsInHand;
            }
            // Add cut card for counting points
            player.Hand.CardsInHand.Add(Deck.CutCard);
            // Show the hand if it isn't already being displayed
            pointsInHand = player.Hand.PointsInHand;
            // Add the points to the player's total
            player.TotalPoints += pointsInHand;

            // Track statistics of the points gained
            if (isCountingCrib)
            {
                player.PlayerStats.NumberOfCribs++;
                player.PlayerStats.TotalPointsGainedFromCribs += pointsInHand;
            }
            else
            {
                player.PlayerStats.NumberOfHands++;
                player.PlayerStats.TotalPointsGainedFromHands += pointsInHand;
            }

            // TODO: print out logic here
            // Display the points in the hand/crib
            // Check if the hand or crib is being counted
            if (player.Hand.IsCrib)
            {
                Console.WriteLine(player.PlayerName + " scored " + pointsInHand + " points in their crib");
            }
            else
            {
                Console.WriteLine(player.PlayerName + " scored " + pointsInHand + " points in their hand");
            }

            // Check if the player won
            if (player.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN)
            {
                // The player won, so they can't score above 121
                player.TotalPoints = GeneralConstants.POINTS_NEEDED_TO_WIN;

                Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

                // TODO: log metrics, store data, etc
                Console.WriteLine(player.PlayerName + " has won the game!");
                
                // Player won, end game
                return true;
            }

            Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

            // Player didn't win, the hunt continues
            return false;
        }

        /// <summary>
        /// Count any potential points from the previous pegging round play
        /// </summary>
        /// <param name="player">The player who played the previous card during the pegging round</param>
        private bool CountPointsFromPegOfPlayerAndCheckForWin(Player player)
        {
            int pointsFromPlay = 0;

            // Show the hand if it isn't already being displayed
            pointsFromPlay = Play.PointsFromPreviousPlay;
            // Add the points to the player's total
            player.TotalPoints += pointsFromPlay;

            Console.WriteLine("The Count is now " + Play.Count + " after " + player.PlayerName + " played a " + Play.CardsPlayedInRound[Play.CardsPlayedInRound.Count - 1].ToString());
            Console.WriteLine(player.PlayerName + " gained " + pointsFromPlay + " point(s) from the play");

            // Check if the player won
            if (player.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN)
            {
                // The player won, so they can't score above 121
                player.TotalPoints = GeneralConstants.POINTS_NEEDED_TO_WIN;

                Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

                // TODO: log metrics, store data, etc
                Console.WriteLine(player.PlayerName + " has won the game!");

                // Player won, end game
                return true;
            }

            Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

            // Player didn't win, the hunt continues
            return false;
        }


        /// <summary>
        /// Add a single point when a player played the last card in a pegging round
        /// </summary>
        /// <param name="player">The player who played the previous card during the pegging round</param>
        private bool CountPointsFromLastCardOfPlayerAndCheckForWin(Player player)
        {
            int pointsFromPlay = GeneralConstants.POINTS_FROM_LAST_CARD;

            // Add the points to the player's total
            player.TotalPoints += pointsFromPlay;

            Console.WriteLine(player.PlayerName + " gained " + pointsFromPlay + " point from the last card");

            // Check if the player won
            if (player.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN)
            {
                // The player won, so they can't score above 121
                player.TotalPoints = GeneralConstants.POINTS_NEEDED_TO_WIN;

                Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

                // TODO: log metrics, store data, etc
                Console.WriteLine(player.PlayerName + " has won the game!");

                // Player won, end game
                return true;
            }

            Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

            // Player didn't win, the hunt continues
            return false;
        }

        /// <summary>
        /// Add a single point when a player received a Go in a pegging round when the opposing player couldn't play
        /// </summary>
        /// <param name="player">The player who played the previous card during the pegging round</param>
        private bool CountPointsFromGoAndCheckForWin(Player player)
        {
            int pointsFromPlay = GeneralConstants.POINTS_FROM_GO;

            // Add the points to the player's total
            player.TotalPoints += pointsFromPlay;

            Console.WriteLine(player.PlayerName + " gained " + pointsFromPlay + " point from the 'Go'");

            // Check if the player won
            if (player.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN)
            {
                // The player won, so they can't score above 121
                player.TotalPoints = GeneralConstants.POINTS_NEEDED_TO_WIN;

                Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

                // TODO: log metrics, store data, etc
                Console.WriteLine(player.PlayerName + " has won the game!");

                // Player won, end game
                return true;
            }

            Console.WriteLine(player.PlayerName + " has a total score of " + player.TotalPoints);

            // Player didn't win, the hunt continues
            return false;
        }

        /// <summary>
        /// Initialize the pegging round by reseting the count and choosing the first person to play
        /// </summary>
        private void InitializePegging()
        {
            GameControl.CurrentGameState = GameControl.GameState.Pegging.ToString();

            // Reset the pegging round
            Play = new Play();

            // Reset everyone's turn
            foreach (Computer computerPlayer in Players)
            {
                computerPlayer.IsTurn = false;
            }

            // Select the first player to peg, which is the non-dealer
            Player nonDealer = Players.Single(a => !a.IsDealer);
            Player Dealer = Players.Single(a => a.IsDealer);
            nonDealer.IsTurn = true;
            Dealer.IsTurn = false;
        }

        /// <summary>
        /// Resets the count and the cards played in the current pegging round back to none
        /// </summary>
        private void ResetPeggingRound()
        {
            // Reset the pegging round
            Play = new Play();
        }

        #endregion

        #region Debug

        #endregion
    }
}
