﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    ///
    /// TODO: link to base class and move common functionality there
    /// TODO: should this be combined with the Hand.cs class? Could simply add a boolean to it
    ///
    public class Crib
    {
        #region Constants

        #endregion

        #region Properties

        public Hand CribHand
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Crib()
        {
            CribHand = new Hand(true);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
