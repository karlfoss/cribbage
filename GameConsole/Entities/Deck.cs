﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// TODO: link to base class and move common functionality there
    /// When to use 'override' or 'new' key words
    /// Summary: override for extending, and 'new' for hiding
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/knowing-when-to-use-override-and-new-keywords
    /// </summary>
    public class Deck
    {
        #region Constants

        #endregion

        #region Properties

        //All of the cards in the deck
        public Stack<Card> DeckOfCards
        {
            get; set;
        }

        //Card cut from the deck after putting cards in the crib
        public Card CutCard
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Constructor for deck including the visual elements
        /// Loads 52 standard cards in a deck, and then shuffles them
        /// </summary>
        public Deck()
        {
            DeckOfCards = new Stack<Card>();
            NewDeck();
        }

        /// <summary>
        /// Draws a card for the cut card
        /// player choose the cut card
        /// </summary>
        /// <param name="game">The current game</param>
        public void CutCardForDeck()
        {
            CutCard = DrawCard();
            CutCard.IsCutCard = true;
        }

        /// <summary>
        /// Shuffles the cards by switching 2 cards at 'random' multiple times
        /// </summary>
        public void Shuffle()
        {
            Random rn = new Random();
            int card1;
            int card2;
            List<Card> shuffledDeckOfCards = DeckOfCards.ToList();

            for (int i = 0; i < GeneralConstants.NUMBER_OF_SHUFFLES; i++)
            {
                card1 = rn.Next(0, DeckOfCards.Count);
                card2 = rn.Next(0, DeckOfCards.Count);

                Card tempCard = shuffledDeckOfCards[card1];
                shuffledDeckOfCards[card1] = shuffledDeckOfCards[card2];
                shuffledDeckOfCards[card2] = tempCard;
            }

            // Put the cards back into the stack
            DeckOfCards.Clear();
            for (int i = 0; i < shuffledDeckOfCards.Count; i++)
            {
                DeckOfCards.Push(shuffledDeckOfCards[i]);
            }
        }

        /// <summary>
        /// Returns the top card from the deck(removing it from the deck in the process)
        /// </summary>
        /// <returns></returns>
        public Card DrawCard()
        {
            return DeckOfCards.Pop();
        }

        /// <summary>
        /// Creates a new deck with the basic 52 cards
        /// </summary>
        public void NewDeck()
        {
            // Burn the deck before loading a new one
            DeckOfCards.Clear();

            // Go through each suit
            for (int i = 0; i < GeneralConstants.NUMBER_OF_CARDS_PER_SUIT; i++)
            {
                // Go through all 13 cards per suit
                for (int j = 0; j < GeneralConstants.NUMBER_OF_SUITS; j++)
                {
                    Card newCard = new Card();

                    newCard.Suit = j;
                    newCard.Type = i;
                    newCard.Value = DeckOfCards.Count;

                    // Ten, Jack, Queen, and King all worth 10 points
                    if (i > GeneralConstants.LOWEST_CARD_TYPE_WORTH_TEN_POINTS)
                    {
                        newCard.Points = GeneralConstants.TEN_POINT_CARD;
                    }
                    else
                    {
                        //j (or CARD Type) is an index that starts at 0, but the lowest point value for a card is 1 (Ace)
                        newCard.Points = i + 1;
                    }

                    // Add the new card to the deck
                    DeckOfCards.Push(newCard);
                }
            }
        }

        /// <summary>
        /// Removes the passed card from the deck
        /// </summary>
        /// <param name="cardToRemove">Card to remove from the deck</param>
        public void RemoveCardFromDeck(Card cardToRemove)
        {
            List<Card> allCardsInDeck = DeckOfCards.ToList();

            // Remove the card from the list
            if (!allCardsInDeck.Remove(cardToRemove))
            {
                throw new InvalidOperationException("Could not remove " + cardToRemove + " from Deck");
            }

            // Clear out the current deck
            DeckOfCards.Clear();

            // Put the cards back in the deck in the order they were originally in (without the removed card)
            for (int i = allCardsInDeck.Count - 1; i >= 0; i--)
            {
                DeckOfCards.Push(allCardsInDeck[i]);
            }
        }

        /// <summary>
        /// Reset the deck for the new game after a player reaches 121 points
        /// </summary>
        public void ResetForNewGame()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reset the deck for the next round
        /// </summary>
        public void ResetForRound()
        {
            NewDeck();
            Shuffle();
            // TODO: No need to set IsCutCard here, when a card is actually cut this object will change
            CutCard = new Card() { IsCutCard = true };
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
