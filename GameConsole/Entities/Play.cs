﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Game
{
    /// <summary>
    /// TODO: rename this to "The Play" or something else since we are not using pegs in the console version and it is not accurate
    /// TODO: make unit test for every possible scenario below for counting points during the play
    /// </summary>
    public class Play
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// The current count of the pegging round
        /// This is updated after checking for all points from the previous card play
        /// </summary>
        public int Count
        {
            get; set;
        }

        /// <summary>
        /// All cards played in the current round
        /// </summary>
        public List<Card> CardsPlayedInRound
        {
            get; set;
        }


        #endregion

        #region Public Methods

        public Play()
        {
            Count = 0;
            CardsPlayedInRound = new List<Card>();
        }

        /// <summary>
        /// Creates a new "play" based off of the previous play.
        /// This is done because a "play" (as a reference variable) would result in one play affecting another when considering multiple
        /// plays during the game planning step for the AI
        /// </summary>
        /// <param name="play">play to copy</param>
        public Play(Play play)
        {
            Count = play.Count;
            CardsPlayedInRound = new List<Card>();

            // Create copy of each card played in the round so far
            foreach (Card card in play.CardsPlayedInRound)
            {
                Card copiedCard = new Card(card);
                CardsPlayedInRound.Add(copiedCard);
            }
        }

        /// <summary>
        /// The number of points (if any) from the previous play in the pegging round
        /// </summary>
        public int PointsFromPreviousPlay
        {
            get { return CountPointsFromPlay(); }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Count all of the points from the previous play
        /// </summary>
        /// <returns>The number of points from the previous play during the pegging round</returns>
        private int CountPointsFromPlay()
        {
            int totalPointsFromPreviousPlay = 0;

            totalPointsFromPreviousPlay += CountAllFifteenOrThirtyOnes();

            totalPointsFromPreviousPlay += CountRun();

            totalPointsFromPreviousPlay += CountPairs();

            return totalPointsFromPreviousPlay;
        }


        /// <summary>
        /// Counts all of the points from reaching a total of either 15 or 31 during each pegging round
        /// </summary>
        /// <returns>The total points from either a count of 15 or 31</returns>
        private int CountAllFifteenOrThirtyOnes()
        {
            int pointsFromFifteenOrThirtyOnes = 0;
            // The total value of all cards played in pegging round
            int totalPointsPlayed = 0;

            foreach (Card card in CardsPlayedInRound)
            {
                totalPointsPlayed += card.Points;
            }

            // Update the current count
            Count = totalPointsPlayed;

            if (Count == GeneralConstants.COUNT_OF_31)
            {
                pointsFromFifteenOrThirtyOnes += GeneralConstants.POINTS_FROM_THIRTY_ONE;
            }
            else if (Count == GeneralConstants.COUNT_OF_15)
	        {
                pointsFromFifteenOrThirtyOnes += GeneralConstants.POINTS_FROM_FIFTEEN;
	        }

            return pointsFromFifteenOrThirtyOnes;
        }

        /// <summary>
        /// Count the highest possible run created from playing the previous card in the pegging round
        /// 3 Card run = 3 points, 4 Card run = 4 points, etc 
        /// </summary>
        /// <returns>The number of points from the highest possible run</returns>
        private int CountRun()
        {
            int pointsFromRuns = 0;

            // Need at least 3 cards for a run, so don't bother counting until 3 cards are played for the current pegging round
            if (CardsPlayedInRound.Count <= 2)
            {
                return pointsFromRuns;
            }

            // Check for the highest possible run first, then look for the smaller and smaller runs
            for (int numberOfCardsInPossibleRun = CardsPlayedInRound.Count; numberOfCardsInPossibleRun >= GeneralConstants.NUMBER_OF_CARDS_IN_RUN_OF_THREE; numberOfCardsInPossibleRun--)
            {
                // Get all of the cards being checked for a possible run
                // TODO: WATCH OUT FOR THIS>>>>MAKE SURE THAT THE ORDER OF THE CARDS PLAYED DOES NOT CHANGE
                List<Card> possibleRunOfCards = CardsPlayedInRound.GetRange(CardsPlayedInRound.Count - numberOfCardsInPossibleRun, numberOfCardsInPossibleRun);
                if (IsRun(possibleRunOfCards))
                {
                    pointsFromRuns += numberOfCardsInPossibleRun;
                    return pointsFromRuns;
                }
            }

            // no runs found
            return pointsFromRuns;
        }

        /// <summary>
        /// Counts all of the points from pairs of cards
        /// 2 of a kind = 2 points
        /// 3 of a kind = 6 points (3 pairs)
        /// 4 of a kind = 12 points (6 pairs)
        /// </summary>
        /// <returns>The number of points from all pairs counted</returns>
        public int CountPairs()
        {
            int numberOfPointsFromPlay = 0;
            int numberOfCardsMatchingInARow = 0;
            Card lastCardPlayed = CardsPlayedInRound[CardsPlayedInRound.Count - 1];

            // Count the number of points from doubles (subtract by 2 because the first card is last card played)
            for (int i = CardsPlayedInRound.Count - 2; i >= 0; i--)
            {
                if (CardsPlayedInRound[i].Type == lastCardPlayed.Type)
                {
                    numberOfCardsMatchingInARow++;
                }
                else
                {
                    // Cards didn't match and the series (if any) is broken
                    break;
                }
            }

            if (numberOfCardsMatchingInARow == 1)
            {
                numberOfPointsFromPlay += 2;
            }
            else if (numberOfCardsMatchingInARow == 2)
            {
                numberOfPointsFromPlay += 6;
            }
            else if (numberOfCardsMatchingInARow == 3)
            {
                numberOfPointsFromPlay += 12;
            }

            return numberOfPointsFromPlay;
        }

        /// <summary>
        /// Sort the cards and check if there is a run
        /// </summary>
        /// <param name="cards">The cards to check for a run with</param>
        /// <returns>True if there is run, false otherwise</returns>
        private bool IsRun(List<Card> cards)
        {
            // TODO: KEF make a common query (probably private method in this class) for this
            // TODO: KEF note that this probably isn't as efficient as making an IComparable interface for Card, 
            // buuut hands are also only 5 length...sooo
            List<Card> sortedCards = cards.OrderBy(a => a.Value).ToList();

            // Don't check the last Card because the card before that already compared with it
            for (int i = 0; i < sortedCards.Count - 1; i++)
            {
                // Run is not possible if the second is not one type higher than the first card
                if (sortedCards[i].Type != sortedCards[i + 1].Type - 1)
                {
                    return false;
                }
            }

            // If all cards were within in 1 type of each other, then a run does exist
            return true;
        }

        #endregion
    }
}
