﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// </summary>
    public class Human : Player
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public Human(string playerName): base(playerName)
        {
            Hand = new Hand(false);
            TotalPoints = 0;
            IsHuman = true;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
