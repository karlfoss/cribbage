﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// </summary>
    public class GreedyComputer : Computer
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public GreedyComputer(string playerName, int difficultyLevel) : base(playerName, difficultyLevel)
        {
            //Calls the base constructor
        }

        ///TODO: upgrade the greedy AI to choose the hand with the most immediate points in it (tie break goes to hand with most potential)
        /// <summary>
        /// Simply finds the cards to discard in to the crib by calculating the best mean for each of the 15
        /// possible hand combinations against the 46 other cards that could be cut for the deck.
        /// Does not consider who's crib or pegging making it a rather weak AI
        /// </summary>
        /// <returns></returns>
        public override List<Card> GetCardsToPutInCrib()
        {
            Dictionary<List<Card>, double> allPossibleHandsWithProjectedPoints = new Dictionary<List<Card>, double>();
            Dictionary<List<Card>, int[]> allPossiblePointsTest = new Dictionary<List<Card>, int[]>();
            Deck deck = new Deck();
            //TODO: KEF consider overriding .equals method for cards
            //TODO: KEF gather other stats on other hands, the range of possible outcomes for the chosen hand, mode of best hand, mean of all possible hands and other??? Good stuff here
            List<Card> cardsNotDealt = deck.DeckOfCards.Where(a => Hand.CardsInHand.All(b => b.Value != a.Value)).ToList();

            List<bool[]> combinationsToCheckForBestHand = CalculatorService.GetAllPossibleCombinations(GeneralConstants.NUMBER_OF_CARDS_DEALT, GeneralConstants.NUMBER_OF_CARDS_KEPT);

            foreach (bool[] combinationToCheckForBestHand in combinationsToCheckForBestHand)
            {
                //Average points for the hand with all possible cards drawn
                double averagePointsForHand = 0;
                List<Card> possibleHand = Hand.RetrieveCardsFromCombination(combinationToCheckForBestHand);

                foreach (Card cardNotDealt in cardsNotDealt)
                {
                    Hand checkHand = new Hand(true);
                    foreach (Card cardInPossibleHand in possibleHand)
                    {
                        checkHand.CardsInHand.Add(cardInPossibleHand);
                    }
                    cardNotDealt.IsCutCard = true;
                    checkHand.CardsInHand.Add(cardNotDealt);
                    averagePointsForHand += checkHand.PointsInHand;
                }

                averagePointsForHand = averagePointsForHand / GeneralConstants.NUMBER_OF_CARDS_NOT_IN_HAND;
                allPossibleHandsWithProjectedPoints.Add(possibleHand, averagePointsForHand);
            }

            //TODO: check for ties, and then base it off of crib points somehow? Potentially give higher rating to dangerous/beneficial
            //TODO: consider using better judgment than the MaxAverage, MaxMedian might be better as it wouldn't get skewed for instance
            List<Card> bestHand = allPossibleHandsWithProjectedPoints.FirstOrDefault(a => a.Value == allPossibleHandsWithProjectedPoints.Values.Max()).Key;

            // Remove other 2 cards from the hand and add them to the crib
            List<Card> cardsDiscardedToCrib = Hand.CardsInHand.Where(a => bestHand.All(b => b.Value != a.Value)).ToList();

            // Mark cards chosen as selected
            foreach (Card card in cardsDiscardedToCrib)
            {
                card.IsSelected = true;
            }

            return cardsDiscardedToCrib;
        }

        public override Card GetCardForPlay(Play play, int numberOfOpponentMovesLeft)
        {
            Card cardToPlay = new Card();

            cardToPlay = CheckMove(play);
            // Can no longer play this card since it was used for pegging once now
            cardToPlay.IsPlayed = true;

            return cardToPlay;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Helper method to start off the decision making process for each possible play
        /// </summary>
        /// <param name="play">The current play</param>
        /// <returns>The card to play</returns>
        private Card CheckMove(Play play)
        {
            // Create a copy of the current hand to avoid reference variable issues
            Hand currentHand = new Hand(Hand);
            List<MinMax> allPossibleMoves = new List<MinMax>();

            List<Card> cardsNotPlayed = currentHand.CardsInHand.Where(a => !a.IsPlayed && ((a.Points + play.Count) <= GeneralConstants.COUNT_OF_31)).ToList();
            if (cardsNotPlayed.Count == 0)
            {
                throw new InvalidOperationException("No valid cards to play");
            }

            // Go through all possible moves
            foreach (Card card in cardsNotPlayed)
            {
                // Create a copy of the current play to avoid reference variable issues
                Play newPlay = new Play(play);
                MinMax minMax = new MinMax();

                newPlay.CardsPlayedInRound.Add(card);
                minMax = CheckMove(newPlay, card);
                minMax.cardPlayed = card;
                allPossibleMoves.Add(minMax);
            }

            // Return the Max if it is my Hand
            // TODO: how to handle ties
            double max = allPossibleMoves.Max(a => a.Max);
            MinMax bestMove = allPossibleMoves.First(a => a.Max == max);

            return Hand.CardsInHand.First(a => a.Value == bestMove.cardPlayed.Value);
        }

        // TODO: Consider making one long list of plays so that every play sequence can be analyzed for troubleshooting

        /// <summary>
        /// TODO: need a helper method for the first round setup
        /// TODO: watch out for object references carrying data used for the actual game-play (in particular, the play object and cards in hand WILL have to be handled carefully)
        /// Implements a MiniMax Game Theory Algorithm https://en.wikipedia.org/wiki/Minimax
        /// TODO: can implement Expectiminimax by multiplying each choice of the opponent by the possibility they have that card and then play it
        /// TODO: storing the ~14000 different hands and their range of outcomes might give a hint what to play as well
        /// </summary>
        /// <param name="play"></param>
        /// <param name="cardPlayed"></param>
        /// <param name="currentPlayersHand"></param>
        /// <param name="numberOfOpponentMoves"></param>
        /// <param name="isMyTurn"></param>
        /// <returns></returns>
        private MinMax CheckMove(Play play, Card cardPlayed)
        {
            List<MinMax> allPossibleMoves = new List<MinMax>();
            int pointsFromPreviousPlay = 0;
            MinMax minMaxForTurn = new MinMax();

            // Count all points earned from the lastCardPlayed + add it to the list of played cards
            pointsFromPreviousPlay = play.PointsFromPreviousPlay;

            // TODO: insert logic for GO/Last Card
            minMaxForTurn.Max += pointsFromPreviousPlay;

            return minMaxForTurn;
        }

        #endregion
    }
}
