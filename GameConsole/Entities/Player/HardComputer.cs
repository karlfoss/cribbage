﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// </summary>
    public class HardComputer : Computer
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public HardComputer(string playerName, int difficultyLevel) : base(playerName, difficultyLevel)
        {
            //Calls the base constructor
        }

        public override List<Card> GetCardsToPutInCrib()
        {
            throw new NotImplementedException();
        }

        public override Card GetCardForPlay(Play play, int numberOfOpponentMovesLeft)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
