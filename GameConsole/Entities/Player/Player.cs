﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// 
    /// NOTE: Abstract documentation: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// </summary>
    public abstract class Player
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// The players current hand
        /// </summary>
        public Hand Hand
        {
            get; set;
        }

        /// <summary>
        /// The total points the player has, 121 needed to win
        /// </summary>
        public int TotalPoints
        {
            get; set;
        }

        /// <summary>
        /// True if the player is the dealer for the round
        /// </summary>
        public bool IsDealer
        {
            get; set;
        }

        /// <summary>
        /// True if the player is human, false if it is computer ai
        /// </summary>
        public bool IsHuman
        {
            get; set;
        }

        /// <summary>
        /// True if its the players turn to play in the pegging round
        /// </summary>
        public bool IsTurn
        {
            get; set;
        }

        /// <summary>
        /// The statistics associated with a particular player
        /// </summary>
        public PlayerStats PlayerStats
        {
            get; set;
        }

        /// <summary>
        /// Player Name
        /// </summary>
        public string PlayerName
        {
            get; set;
        }

        #endregion

        #region Public Methods

        //TODO: make a constructor for common classes
        public Player(string playerName)
        {
            // Assign the player a name for logging and identification purposes
            PlayerName = playerName;
            PlayerStats = new PlayerStats();
        }

        /// <summary>
        /// Reset the player for the new game after a player reaches 121 points
        /// </summary>
        public void ResetForNewGame()
        {
            //Remove the hand for the display and clear previous hand
            Hand = new Hand(false);

            //Reset Points
            TotalPoints = 0;
        }

        /// <summary>
        /// Reset the player for the next round
        /// </summary>
        public void ResetForRound()
        {
            // Remove the cards to 'put back in deck' and clear hand
            Hand = new Hand(false);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
