﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base computer player which has all common ai/computer functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// 
    /// NOTE: constructor for base classes: https://stackoverflow.com/questions/3873343/can-i-inherit-constructors
    /// NOTE: uses strategy design pattern: http://www.dofactory.com/net/strategy-design-pattern
    ///     for implementing different algorithms for lower/higher difficulties
    /// NOTE: why use abstract https://stackoverflow.com/questions/2118055/how-to-force-overriding-a-method-in-a-descendant-without-having-an-abstract-bas
    /// </summary>
    public abstract class Computer : Player
    {
        #region Constants

        public const int EASY_DIFFICULTY = 1;
        public const int GREEDY_DIFFICULTY = 2;
        public const int NORMAL_DIFFICULTY = 3;
        public const string EASY_DIFFICULTY_STRING = "1";
        public const string GREEDY_DIFFICULTY_STRING = "2";
        public const string NORMAL_DIFFICULTY_STRING = "3";

        public enum ComputerDifficultyChoices
        {
            Easy = 1,
            Greedy = 2,
            Normal = 3
        }

        #endregion

        #region Properties

        /// <summary>
        /// Difficulty level of the computer player
        /// </summary>
        public int DifficultyLevel
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Computer(string playerName, int difficultyLevel) : base(playerName)
        {
            Hand = new Hand(false);
            TotalPoints = 0;
            IsHuman = false;
            DifficultyLevel = difficultyLevel;
        }

        /// <summary>
        /// Chooses which cards to be discarded to the crib
        /// </summary>
        /// <returns></returns>
        public abstract List<Card> GetCardsToPutInCrib();

        public abstract Card GetCardForPlay(Play play, int numberOfOpponentMovesLeft);

        #endregion

        #region Private Methods

        #endregion
    }
}
