﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// </summary>
    public class EasyComputer : Computer
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public EasyComputer(string playerName, int difficultyLevel) : base(playerName, difficultyLevel)
        {
            //Calls the base constructor
        }

        /// <summary>
        /// Simply finds the cards to discard in to the crib by calculating the best mean for each of the 15
        /// possible hand combinations against the 46 other cards that could be cut for the deck.
        /// Does not consider who's crib or pegging making it a rather weak AI
        /// </summary>
        /// <returns></returns>
        public override List<Card> GetCardsToPutInCrib()
        {
            Dictionary<List<Card>, double> allPossibleHandsWithProjectedPoints = new Dictionary<List<Card>, double>();
            Dictionary<List<Card>, int[]> allPossiblePointsTest = new Dictionary<List<Card>, int[]>();
            Deck deck = new Deck();
            //TODO: KEF consider overriding .equals method for cards
            //TODO: KEF gather other stats on other hands, the range of possible outcomes for the chosen hand, mode of best hand, mean of all possible hands and other??? Good stuff here
            List<Card> cardsNotDealt = deck.DeckOfCards.Where(a => Hand.CardsInHand.All(b => b.Value != a.Value)).ToList();

            List<bool[]> combinationsToCheckForBestHand = CalculatorService.GetAllPossibleCombinations(GeneralConstants.NUMBER_OF_CARDS_DEALT, GeneralConstants.NUMBER_OF_CARDS_KEPT);

            foreach (bool[] combinationToCheckForBestHand in combinationsToCheckForBestHand)
            {
                //Average points for the hand with all possible cards drawn
                double averagePointsForHand = 0;
                List<Card> possibleHand = Hand.RetrieveCardsFromCombination(combinationToCheckForBestHand);

                foreach (Card cardNotDealt in cardsNotDealt)
                {
                    Hand checkHand = new Hand(true);
                    foreach (Card cardInPossibleHand in possibleHand)
                    {
                        checkHand.CardsInHand.Add(cardInPossibleHand);
                    }
                    cardNotDealt.IsCutCard = true;
                    checkHand.CardsInHand.Add(cardNotDealt);
                    averagePointsForHand += checkHand.PointsInHand;
                }

                averagePointsForHand = averagePointsForHand / GeneralConstants.NUMBER_OF_CARDS_NOT_IN_HAND;
                allPossibleHandsWithProjectedPoints.Add(possibleHand, averagePointsForHand);
            }

            //TODO: check for ties, and then base it off of crib points somehow? Potentially give higher rating to dangerous/beneficial
            //TODO: consider using better judgment than the MaxAverage, MaxMedian might be better as it wouldn't get skewed for instance
            List<Card> bestHand = allPossibleHandsWithProjectedPoints.FirstOrDefault(a => a.Value == allPossibleHandsWithProjectedPoints.Values.Max()).Key;

            // Remove other 2 cards from the hand and add them to the crib
            List<Card> cardsDiscardedToCrib = Hand.CardsInHand.Where(a => bestHand.All(b => b.Value != a.Value)).ToList();

            // Mark cards chosen as selected
            foreach (Card card in cardsDiscardedToCrib)
            {
                card.IsSelected = true;
            }

            return cardsDiscardedToCrib;
        }

        /// <summary>
        /// This 'easy' AI just chooses a random card for each play
        /// </summary>
        /// <param name="play">The play round information</param>
        /// <param name="numberOfOpponentMovesLeft">NOT USED with this difficulty setting</param>
        /// <returns></returns>
        public override Card GetCardForPlay(Play play, int numberOfOpponentMovesLeft)
        {
            Card cardToPlay;

            // Get all of the cards that haven't yet been used for pegging and are valid plays
            List<Card> cardsNotPlayed = Hand.CardsInHand.Where(a => !a.IsPlayed && (a.Points + play.Count <= 31)).ToList();

            if (cardsNotPlayed.Count == 0)
            {
                throw new InvalidOperationException("No valid cards to play");
            }

            //Randomly choose card to play that hasn't been played yet
            Random rn = new Random();

            cardToPlay = cardsNotPlayed[(rn.Next(0, cardsNotPlayed.Count - 1))];

            // Can no longer play this card since it was used for pegging once now
            cardToPlay.IsPlayed = true;

            return cardToPlay;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
