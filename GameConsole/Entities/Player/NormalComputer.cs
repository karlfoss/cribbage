﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// TODO: see virtual TIIIIIIIIISSSSSS -> https://www.tutorialspoint.com/csharp/csharp_polymorphism.htm
    /// </summary>
    public class NormalComputer : Computer
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public NormalComputer(string playerName, int difficultyLevel) : base(playerName, difficultyLevel)
        {
            // Calls the base constructor
        }

        /// <summary>
        /// Selects which cards into the crib
        /// </summary>
        public override List<Card> GetCardsToPutInCrib()
        {
            Dictionary<List<Card>, double> allPossibleHandsWithProjectedPoints = new Dictionary<List<Card>, double>();
            Dictionary<List<Card>, int[]> allPossiblePointsTest = new Dictionary<List<Card>, int[]>();
            Deck deck = new Deck();
            //TODO: KEF consider overriding .equals method for cards
            //TODO: KEF gather other stats on other hands, the range of possible outcomes for the chosen hand, mode of best hand, mean of all possible hands and other??? Good stuff here
            List<Card> cardsNotDealt = deck.DeckOfCards.Where(a => Hand.CardsInHand.All(b => b.Value != a.Value)).ToList();

            List<bool[]> combinationsToCheckForBestHand = CalculatorService.GetAllPossibleCombinations(GeneralConstants.NUMBER_OF_CARDS_DEALT, GeneralConstants.NUMBER_OF_CARDS_KEPT);

            foreach (bool[] combinationToCheckForBestHand in combinationsToCheckForBestHand)
            {
                //Average points for the hand with all possible cards drawn
                double averagePointsForHand = 0;
                List<Card> possibleHand = Hand.RetrieveCardsFromCombination(combinationToCheckForBestHand);

                foreach (Card cardNotDealt in cardsNotDealt)
                {
                    Hand checkHand = new Hand(true);
                    foreach (Card cardInPossibleHand in possibleHand)
                    {
                        checkHand.CardsInHand.Add(cardInPossibleHand);
                    }
                    cardNotDealt.IsCutCard = true;
                    checkHand.CardsInHand.Add(cardNotDealt);
                    averagePointsForHand += checkHand.PointsInHand;
                }

                averagePointsForHand = averagePointsForHand / GeneralConstants.NUMBER_OF_CARDS_NOT_IN_HAND;
                allPossibleHandsWithProjectedPoints.Add(possibleHand, averagePointsForHand);
            }

            //TODO: check for ties, and then base it off of crib points somehow? Potentially give higher rating to dangerous/beneficial
            //TODO: consider using better judgment than the MaxAverage, MaxMedian might be better as it wouldn't get skewed for instance
            List<Card> bestHand = allPossibleHandsWithProjectedPoints.FirstOrDefault(a => a.Value == allPossibleHandsWithProjectedPoints.Values.Max()).Key;

            // Remove other 2 cards from the hand and add them to the crib
            List<Card> cardsDiscardedToCrib = Hand.CardsInHand.Where(a => bestHand.All(b => b.Value != a.Value)).ToList();

            // Mark cards chosen as selected
            foreach (Card card in cardsDiscardedToCrib)
            {
                card.IsSelected = true;
            }

            return cardsDiscardedToCrib;
        }

        public override Card GetCardForPlay(Play play, int numberOfOpponentMovesLeft)
        {
            Card cardToPlay = new Card();

            cardToPlay = CheckMove(play, numberOfOpponentMovesLeft);
            // Can no longer play this card since it was used for pegging once now
            cardToPlay.IsPlayed = true;

            return cardToPlay;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Helper method to start off the decision making process for each possible play
        /// </summary>
        /// <param name="play">The current play</param>
        /// <returns>The card to play</returns>
        private Card CheckMove(Play play, int numberOfOpponentMovesLeft)
        {
            //NOTE: This below line is key. How much depth should be considered when making a move??
            //TODO: consider limiting the depth of search, especially without a algorithm that considers the possibility an opponent has and plays a card. IE, I get 2 plays, and opponent gets 1
            numberOfOpponentMovesLeft = 1;

            //TODO: add a variable that keeps track of all revealed cards and logic that ensures there are only 4 cards of a type in existence
            // Create a copy of the current hand to avoid reference variable issues
            Hand currentHandBreadth = new Hand(Hand);
            bool isMyTurn = true;

            List<MinMax> allPossibleMoves = new List<MinMax>();
            List<Card> cardsNotPlayed = currentHandBreadth.CardsInHand.Where(a => a.IsPlayed == false && ((a.Points + play.Count) <= GeneralConstants.COUNT_OF_31)).ToList();
            if (cardsNotPlayed.Count == 0)
            {
                throw new InvalidOperationException("No valid cards to play");
            }

            // Go through all possible moves
            foreach (Card card in cardsNotPlayed)
            {
                // Create a copy of the current play to avoid reference variable issues
                Play newPlay = new Play(play);
                MinMax minMax = new MinMax();
                Hand currentHandDepth = new Hand(Hand);
                Card cardToPlay = currentHandDepth.CardsInHand.Single(a => a.Value == card.Value);
                // NOTE: It is imperative this also changes the card in the current hand as well that is being passed to prevent duplicates
                cardToPlay.IsPlayed = true;
                newPlay.CardsPlayedInRound.Add(cardToPlay);
                //TODO: the below should be a static method in a separate class (THAT DOES NOT HAVE ACCESS TO THE PLAYER OBJECT)
                //      This is based off of the principle separation of concerns. I don't want these methods to affect the existing player instance. They should be completely independent
                minMax = CheckMove(newPlay, cardToPlay, currentHandDepth, numberOfOpponentMovesLeft, isMyTurn);
                //TODO: find out why I have to set the card here
                minMax.cardPlayed = card;
                allPossibleMoves.Add(minMax);
            }

            // Return the Max if it is my Hand
            // TODO: how to handle ties
            double netMax = allPossibleMoves.Max(a => a.Max + a.Min);
            MinMax bestMove = allPossibleMoves.First(a => (a.Max + a.Min) == netMax);

            // TODO: add choosing logic here
            // TODO: have to return the actual card here, not the copy. Or select the card to return based off of the copy?
            // TODO: Is this the best way to select the cards that are supposed to be identical?
            return Hand.CardsInHand.First(a => a.Value == bestMove.cardPlayed.Value);
        }

        // TODO: Consider making one long list of plays so that every play sequence can be analyzed for troubleshooting

        /// <summary>
        /// TODO: need a helper method for the first round setup
        /// TODO: watch out for object references carrying data used for the actual game-play (in particular, the play object and cards in hand WILL have to be handled carefully)
        /// Implements a MiniMax Game Theory Algorithm https://en.wikipedia.org/wiki/Minimax
        /// TODO: can implement Expectiminimax by multiplying each choice of the opponent by the possibility they have that card and then play it
        /// TODO: storing the ~14000 different hands and their range of outcomes might give a hint what to play as well
        /// </summary>
        /// <param name="play"></param>
        /// <param name="cardPlayed"></param>
        /// <param name="currentPlayersHand"></param>
        /// <param name="numberOfOpponentMoves"></param>
        /// <param name="isMyTurn"></param>
        /// <returns></returns>
        private MinMax CheckMove(Play play, Card cardPlayed, Hand currentHandBreadth, int numberOfOpponentMovesLeft, bool isMyTurn)
        {
            List<MinMax> allPossibleMoves = new List<MinMax>();
            int pointsFromPreviousPlay = 0;
            MinMax minMaxForTurn = new MinMax();

            // Count all points earned from the lastCardPlayed + add it to the list of played cards
            pointsFromPreviousPlay = play.PointsFromPreviousPlay;

            // TODO: insert logic for GO/Last Card
            // Return after all 8 cards have been played
            // TODO: make constant here for the 8
            // TODO: enhance this to consider Gos and Last cards in the future
            if (isMyTurn)
            {
                minMaxForTurn.Max += pointsFromPreviousPlay;
            }
            else
            {
                minMaxForTurn.Min -= pointsFromPreviousPlay;
            }

            bool iCanPlayThisRound = currentHandBreadth.CardsInHand.Where(a => a.IsPlayed == false && a.Points + play.Count <= GeneralConstants.COUNT_OF_31).Count() > 0;
            bool otherPlayerCanPlayThisRound = numberOfOpponentMovesLeft > 0;
            //bool currentPlayerCanPlayNextRound = currentPlayer.Hand.CardsInHand.Where(a => !a.IsPlayed).ToList().Count > 0;
            //bool nextPlayerCanPlayNextRound = nextPlayer.Hand.CardsInHand.Where(a => !a.IsPlayed).ToList().Count > 0;
            bool isCount31 = GeneralConstants.COUNT_OF_31 == play.Count;

            // TODO: consider doing a do/while here instead (this would allow moving the moving of the initializing pegging into this method, and reduce recursion)
            if (isCount31)
            {
                return minMaxForTurn;
            }
            else if (isMyTurn && otherPlayerCanPlayThisRound)
            {
                isMyTurn = false;
            }
            else if (isMyTurn && !otherPlayerCanPlayThisRound && iCanPlayThisRound)
            {
                // Don't change turns since the next player can't play
            }
            else if (isMyTurn && !otherPlayerCanPlayThisRound && !iCanPlayThisRound)
            {
                return minMaxForTurn;
            }
            else if (!isMyTurn && iCanPlayThisRound)
            {
                isMyTurn = true;
            }
            else if (!isMyTurn && !iCanPlayThisRound && otherPlayerCanPlayThisRound)
            {
                // I can't play so don't switch turns
            }
            else if (!isMyTurn && !otherPlayerCanPlayThisRound && !otherPlayerCanPlayThisRound)
            {
                return minMaxForTurn;
            }
            else
            {
                throw new InvalidOperationException("Unknown play state in MinMax tree");
            }

            // Get list of all moves you can do if it is your turn and choose the best Max
            if (isMyTurn)
            {
                List<Card> legalCardsLeftToPlayInRound = currentHandBreadth.CardsInHand.Where(a => a.IsPlayed == false && ((a.Points + play.Count) <= GeneralConstants.COUNT_OF_31)).ToList();
                // Go through all possible moves
                foreach (Card card in legalCardsLeftToPlayInRound)
                {
                    // Create a copy of the current play to avoid reference variable issues
                    Play newPlay = new Play(play);
                    MinMax minMax = new MinMax();
                    Hand currentHandDepth = new Hand(currentHandBreadth);
                    Card cardToPlay = currentHandDepth.CardsInHand.Single(a => a.Value == card.Value);
                    // NOTE: It is imperative this also changes the card in the current hand as well that is being passed to prevent duplicates
                    cardToPlay.IsPlayed = true;
                    newPlay.CardsPlayedInRound.Add(cardToPlay);
                    //TODO: the below should be a static method in a separate class (THAT DOES NOT HAVE ACCESS TO THE PLAYER OBJECT)
                    //      This is based off of the principle separation of concerns. I don't want these methods to affect the existing player instance. They should be completely independent
                    minMax = CheckMove(newPlay, cardToPlay, currentHandDepth, numberOfOpponentMovesLeft, isMyTurn);
                    minMax.cardPlayed = cardToPlay;
                    minMax.Max += minMaxForTurn.Max;
                    minMax.Min += minMaxForTurn.Min;
                    allPossibleMoves.Add(minMax);
                }
            }
            // Get list of all moves opponent can do it if is their turn
            else
            {
                List<Card> allValidCardPlaysInRoundForOpponent = Card.CardGenerator(GeneralConstants.COUNT_OF_31 - play.Count);
                foreach (Card card in allValidCardPlaysInRoundForOpponent)
                {
                    // Create a copy of the current play to avoid reference variable issues
                    Play newPlay = new Play(play);
                    MinMax minMax = new MinMax();

                    // TODO: is a currentHandDepth needed since a Breadth card should never be used at the same level and playing no card effectively preserves the level. Or I can be redundant, still do the currentHandDepth for the sake of consistency
                    //Hand currentHandDepth = new Hand(currentHandBreadth);
                    //Card cardToPlay = currentHandDepth.CardsInHand.Single(a => a.Value == card.Value);
                    // NOTE: It is imperative this also changes the card in the current hand as well that is being passed to prevent duplicates
                    card.IsPlayed = true;
                    newPlay.CardsPlayedInRound.Add(card);

                    //TODO: the below should be a static method in a separate class (THAT DOES NOT HAVE ACCESS TO THE PLAYER OBJECT)
                    //      This is based off of the principle separation of concerns. I don't want these methods to affect the existing player instance. They should be completely independent
                    minMax = CheckMove(newPlay, card, currentHandBreadth, numberOfOpponentMovesLeft - 1, isMyTurn);
                    minMax.cardPlayed = card;
                    minMax.Max += minMaxForTurn.Max;
                    minMax.Min += minMaxForTurn.Min;
                    allPossibleMoves.Add(minMax);
                }
            }

            // Return the Max if it is my Hand
            // TODO: is the net different between the 2 the ideal way to determine what to return
            if (isMyTurn)
            {
                // TODO: how to handle ties
                double netMax = allPossibleMoves.Max(a => a.Max + a.Min);
                return allPossibleMoves.First(a => (a.Max + a.Min) == netMax);
            }
            // Return the Min if the opponent is making the choice
            else
            {
                double netMin = allPossibleMoves.Min(a => a.Max + a.Min);
                return allPossibleMoves.First(a => (a.Max + a.Min) == netMin);
            }
        }

        #endregion
    }
}
