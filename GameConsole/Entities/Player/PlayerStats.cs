﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game
{
    /// <summary>
    /// TODO: the averages can be produced at any time, are they worth storing in a database/object?
    /// The stats associated with each player for record keeping, achivements, and AI purposes
    /// </summary>
    public class PlayerStats
    {
        #region Constants

        #endregion

        #region Properties

        public int TotalPointsGainedFromHands
        {
            get; set;
        }

        public int MaxPointsPerHand
        {
            get; set;
        }

        public int TotalPointsGainedFromCribs
        {
            get; set;
        }

        public int  MaxPointsPerCrib
        {
            get; set;
        }

        public int TotalPointsGainedFromPlay
        {
            get; set;
        }

        public int MaxPointsPerPlay
        {
            get; set;
        }

        /// <summary>
        /// Only counting full playing rounds, will not count if game ends before the play is finished
        /// </summary>
        public int NumberOfPlays
        {
            get; set;
        }

        public int NumberOfHands
        {
            get; set;
        }

        public int NumberOfCribs
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public PlayerStats()
        {
            //TODO: move this to a database for record keeping. Then initialize them differenlty?
            //NOTE: By default, all of the ints/doubles will have a value of 0
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
