﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game
{
    public class MinMax
    {
        #region Constants

        #endregion

        #region Properties

        public double Min { get; set; }

        public double Max { get; set; }

        public Card cardPlayed { get; set; }

        #endregion

        #region Public Methods

        public MinMax()
        {
            Min = 0;
            Max = 0;
            cardPlayed = new Card();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
