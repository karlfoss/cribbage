﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Game
{
    /**
     * TODO: link to base class and move common functionality there
     */
    public class Card
    {
        #region Constants

        // Card Types
        public enum Types
        {
            Ace,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King
        }

        //TODO: add Unicode values for card for console printout if possible

        // Possible Card Suits
        public enum Suits
        {
            Clubs,
            Diamonds,
            Hearts,
            Spades,
            Tuxedos // Fake suit used in situations where the suit is irrelevant, such as, during the Play section
        }

        #endregion

        #region Properties

        // Unique value of the card (IE 0 - 51)
        public int Value
        {
            get; set;
        }

        // The type of card (IE King, Queen, etc)
        public int Type
        {
            get; set;
        }

        // The suit of the card (Spades, Hearts, Diamonds, Clubs)
        public int Suit
        {
            get; set;
        }

        // The number of points the card is worth
        public int Points
        {
            get; set;
        }

        // True if this is the cut from the deck
        public bool IsCutCard
        {
            get; set;
        }

        // True if this card has already been used for played for pegging
        public bool IsPlayed
        {
            get; set;
        }

        /// <summary>
        /// True if this card was selected by a player
        /// Will be true if selected for crib or pegging
        /// </summary>
        public bool IsSelected
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Card()
        {
            Value = -1;
            Suit = -1;
            Type = -1;
            Points = -1;
        }

        /// <summary>
        /// Creates a new card based off of the previous card.
        /// This is done because a card (as a reference variable) would result in one card changing another card upon 
        /// certain actions like being used more than once during the Play round
        /// </summary>
        /// <param name="card">card to copy</param>
        public Card(Card card)
        {
            Value = card.Value;
            Suit = card.Suit;
            Type = card.Type;
            Points = card.Points;
            IsCutCard = card.IsCutCard;
            IsPlayed = card.IsPlayed;
            IsSelected = card.IsSelected;
        }

        // TODO: should this be in a different class???
        public static List<Card> CardGenerator(int highestPossiblePoint)
        {
            // There are no valid plays if there are no points remaining in the count
            if (highestPossiblePoint < 1)
            {
                return new List<Card>();
            }

            List<Card> allValidCardPlays = new List<Card>();

            // Go through each suit
            for (int i = 0; i < GeneralConstants.NUMBER_OF_CARDS_PER_SUIT; i++)
            {
                Card newCard = new Card();

                // Assign a fake suit since this should only be used for the Play round, where suits don't matter
                newCard.Suit = (int)Suits.Tuxedos;
                newCard.Type = i;

                // Ten, Jack, Queen, and King all worth 10 points
                if (i > GeneralConstants.LOWEST_CARD_TYPE_WORTH_TEN_POINTS)
                {
                    newCard.Points = GeneralConstants.TEN_POINT_CARD;
                }
                else
                {
                    //j (or CARD Type) is an index that starts at 0, but the lowest point value for a card is 1 (Ace)
                    newCard.Points = i + 1;
                }

                if (newCard.Points <= highestPossiblePoint)
                {
                    allValidCardPlays.Add(newCard);
                }
            }

            return allValidCardPlays;
        }

        // TODO: override compareTo method at some point to use something besides value for comparisons, which relay on standard deck creation to accurate

        /// <summary>
        /// TODO: should I use new or override here?
        /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/knowing-when-to-use-override-and-new-keywords
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return String.Format(Enum.GetName(typeof(Types), Type) + " of " + Enum.GetName(typeof(Suits), Suit) + " - Value: " + Value + " Points: " + Points);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}

