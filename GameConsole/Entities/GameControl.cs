﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// TODO: consider making list of Enums to keep track of all possible game states per the state design pattern below
    /// TODO: link to base class and move common functionality there
    /// 
    /// State based button that controls the game flow for the human player
    /// 
    /// Relevant Notes:
    /// GameControl uses State design pattern: http://www.dofactory.com/net/state-design-pattern
    /// </summary>
    public class GameControl
    {
        #region Constants

        public enum GameState
        {
            CutForDeal,             // Determines who deals first
            DealCardsToPlayers,     // 6 cards are dealt to each player
            ChooseCardsForCrib,     // Choose the cards that go to the dealer's crib
            CutCardFromDeck,        // Cut a card from the deck that is used for scoring
            Pegging,                // Pegging round
            CountNonDealersHand,    // Count the hand of the non-dealing player first
            CountDealersHand,       // Count the hand of the dealer
            CountCrib               // Count the dealer's crib
        }

        #endregion

        #region Properties

        /// <summary>
        /// The current game state
        /// </summary>
        public string CurrentGameState
        {
            get; set;
        }

        /// <summary>
        /// True only for the first round of game-play so the deal doesn't switch
        /// </summary>
        public bool IsFirstRound
        {
            get; set;
        }

        /// <summary>
        /// False until someone wins the game
        /// </summary>
        public bool WeHaveAWinner
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public GameControl()
        {

        }

        #endregion
    }
}
