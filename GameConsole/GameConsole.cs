﻿using System;
using System.Collections.Generic;
using Game;

namespace GameConsole
{
    class GameConsole
    {
        // The number of simulations to run
        private static int NUMBER_OF_GAMES_TO_PLAY = 10;
        private static int COMPUTER_PLAYER_ONE = 0;
        private static int COMPUTER_PLAYER_TWO = 1;

        // List of Computer Players
        private static IList<Computer> ComputerPlayers = new List<Computer>();

        static void Main(string[] args)
        {
            // Default Computer 1 difficulty to Easy and Computer 2 to Normal
            Computer ComputerOne = new EasyComputer("ComputerOne", Computer.EASY_DIFFICULTY);
            Computer ComputerTwo = new NormalComputer("ComputerTwo", Computer.NORMAL_DIFFICULTY);

            //TODO: find a way to print out cards themselves to allow game play without visuals
            //Console.OutputEncoding = System.Text.Encoding.Unicode;
            ComputerPlayers.Add(ComputerOne);
            ComputerPlayers.Add(ComputerTwo);

            //TODO: create a baseEntity that has a Game object (so everything can update itself??)
            //TODO: move almost all functionality into GameService, and have the gameService reference the eventService
            PromptUserForInput();
        }

        private static void PromptUserForInput()
        {
            bool exitGame = false;

            do
            {
                Console.Clear();
                Console.WriteLine("Number of Simulations to Run: " + NUMBER_OF_GAMES_TO_PLAY);
                Console.WriteLine("Computer 1 Name: " + ComputerPlayers[COMPUTER_PLAYER_ONE].PlayerName);
                Console.WriteLine("Computer 1 Difficulty: " + Enum.GetName(typeof(Computer.ComputerDifficultyChoices), ComputerPlayers[COMPUTER_PLAYER_ONE].DifficultyLevel));
                Console.WriteLine("Computer 2 Name: " + ComputerPlayers[COMPUTER_PLAYER_TWO].PlayerName);
                Console.WriteLine("Computer 2 Difficulty: " + Enum.GetName(typeof(Computer.ComputerDifficultyChoices), ComputerPlayers[COMPUTER_PLAYER_TWO].DifficultyLevel));
                Console.WriteLine("--------------------------");
                // Prompt the user for options
                Console.WriteLine("Options for Cribbage Simulator:");
                Console.WriteLine("\t1 - Run Simulator");
                Console.WriteLine("\t2 - Change Simulator Settings");
                Console.WriteLine("\t3 - Exit");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        // Initialize computer settings
                        InitailizeGame();
                        ComputerVsComputerGame simulation = new ComputerVsComputerGame(ComputerPlayers[COMPUTER_PLAYER_ONE], ComputerPlayers[COMPUTER_PLAYER_TWO]);
                        // Run the full game simulation
                        simulation.PlayGame(NUMBER_OF_GAMES_TO_PLAY);
                        break;
                    case "2":
                        // Add new logic flow after here
                        ChangeSimulationSettings();
                        break;
                    case "3":
                        exitGame = true;
                        break;
                }

            } while (!exitGame);

            Console.WriteLine("Thanks for playing!");
            System.Threading.Thread.Sleep(2000);
        }

        private static void InitailizeGame()
        {
            for (int i = 0; i < ComputerPlayers.Count; i++)
            {
                switch (ComputerPlayers[i].DifficultyLevel)
                {
                    case Computer.EASY_DIFFICULTY:
                        ComputerPlayers[i] = new EasyComputer(ComputerPlayers[i].PlayerName, Computer.EASY_DIFFICULTY);
                        break;
                    case Computer.GREEDY_DIFFICULTY:
                        ComputerPlayers[i] = new GreedyComputer(ComputerPlayers[i].PlayerName, Computer.GREEDY_DIFFICULTY);
                        break;
                    case Computer.NORMAL_DIFFICULTY:
                        ComputerPlayers[i] = new NormalComputer(ComputerPlayers[i].PlayerName, Computer.NORMAL_DIFFICULTY);
                        break;
                }
            }
        }

        private static void ChangeSimulationSettings()
        {
            bool exitGameSettings = false;

            do
            {
                // Prompt the user for options
                Console.Clear();
                Console.WriteLine("Number of Simulations to Run: " + NUMBER_OF_GAMES_TO_PLAY);
                Console.WriteLine("Computer 1 Name: " + ComputerPlayers[COMPUTER_PLAYER_ONE].PlayerName);
                Console.WriteLine("Computer 1 Difficulty: " + Enum.GetName(typeof(Computer.ComputerDifficultyChoices), ComputerPlayers[COMPUTER_PLAYER_ONE].DifficultyLevel));
                Console.WriteLine("Computer 2 Name: " + ComputerPlayers[COMPUTER_PLAYER_TWO].PlayerName);
                Console.WriteLine("Computer 2 Difficulty: " + Enum.GetName(typeof(Computer.ComputerDifficultyChoices), ComputerPlayers[COMPUTER_PLAYER_TWO].DifficultyLevel));
                Console.WriteLine("--------------------------");
                Console.WriteLine("Update Cribbage Simulator:");
                Console.WriteLine("\t1 - Change Number of Simulations");
                Console.WriteLine("\t2 - Change Computer 1 Name");
                Console.WriteLine("\t3 - Change Computer 1 Difficulty");
                Console.WriteLine("\t4 - Change Computer 2 Name");
                Console.WriteLine("\t5 - Change Computer 2 Difficulty");
                Console.WriteLine("\t6 - Exit Settings Changes");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        // Have user enter the number of simulations to run
                        NUMBER_OF_GAMES_TO_PLAY = GetSimulationInput("Number of Simulations");
                        break;
                    case "2":
                        // Have user computer 1 name
                        ComputerPlayers[COMPUTER_PLAYER_ONE].PlayerName = GetStringInput("Computer 1 Name");
                        break;
                    case "3":
                        // Have user enter the computer 1 difficulty
                        GetComputerDifficultyInput("Computer 1 Difficulty", ComputerPlayers[COMPUTER_PLAYER_ONE]);
                        break;
                    case "4":
                        // Have user computer 2 name
                        ComputerPlayers[COMPUTER_PLAYER_TWO].PlayerName = GetStringInput("Computer 2 Name");
                        break;
                    case "5":
                        // Have user enter the computer 2 difficulty
                        GetComputerDifficultyInput("Computer 2 Difficulty", ComputerPlayers[COMPUTER_PLAYER_TWO]);
                        break;
                    case "6":
                        exitGameSettings = true;
                        break;
                }

            } while (!exitGameSettings);
        }

        private static string GetStringInput(string option)
        {
            Console.WriteLine("Please Enter the " + option);
            string optionChange = Console.ReadLine();

            while (string.IsNullOrEmpty(optionChange))
            {
                Console.WriteLine("Please enter a valid " + option);
                optionChange = Console.ReadLine();
            }

            return optionChange;
        }

        private static int GetSimulationInput(string option)
        {
            Console.WriteLine("Please Enter the " + option);
            int optionChange = 0;
            bool validNumberOfSimulations = false;

            do
            {

                while (!int.TryParse(Console.ReadLine(), out optionChange))
                {
                    Console.WriteLine("Please enter a valid integer!");
                }

                // TOOD: make constant for the max number and find a typical time that limit runs for
                if (optionChange > 0 && optionChange <= 200)
                {
                    validNumberOfSimulations = true;
                }
                else if (optionChange > 200)
                {
                    Console.WriteLine("WARNING! It may take a long time to run this many simulations.\nContinuing in 5 seconds...");
                    System.Threading.Thread.Sleep(5000);
                    validNumberOfSimulations = true;
                }
                else
                {
                    Console.WriteLine("Please enter a valid number of simulations! It must be greater than 0.");
                }

            } while (!validNumberOfSimulations);

            return optionChange;
        }

        private static void GetComputerDifficultyInput(string option, Computer computerToUpdate)
        {
            bool validDifficultySet = false;

            do
            {
                Console.Clear();
                Console.WriteLine("Please Enter the " + option);

                // Prompt the user for options
                Console.WriteLine("Options for Computer Difficulty");
                Console.WriteLine("\t1 - Easy - Picks random plays, but chooses hands normally");
                Console.WriteLine("\t2 - Greedy - Picks highest immediate point hands and plays");
                Console.WriteLine("\t3 - Normal - Picks balanced hands and plays");
                Console.WriteLine("\t4 - Exit without changing difficulty");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case Computer.EASY_DIFFICULTY_STRING:
                        computerToUpdate.DifficultyLevel = Computer.EASY_DIFFICULTY;
                        validDifficultySet = true;
                        break;
                    case Computer.GREEDY_DIFFICULTY_STRING:
                        computerToUpdate.DifficultyLevel = Computer.GREEDY_DIFFICULTY;
                        validDifficultySet = true;
                        break;
                    case Computer.NORMAL_DIFFICULTY_STRING:
                        computerToUpdate.DifficultyLevel = Computer.NORMAL_DIFFICULTY;
                        validDifficultySet = true;
                        break;
                    case "4":
                        validDifficultySet = true;
                        break;
                }

            } while (!validDifficultySet);
        }
    }
}
