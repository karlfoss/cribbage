﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core;
using System;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class EntityUnitTests
    {
        private const int NUMBER_OF_CARDS_IN_CRIBBAGE_HAND_WITHOUT_CUT_CARD = 4;

        //TODO: KEF make test cases for each point type (flush, knobs, runs, etc). Crib flush vs normal flush, etc
        
        //TODO: KEF is this technically an integration test
        //TODO: KEF make constants of every possible card and use those to test unique use cases
        //TODO: KEF make constants of hands and decks too for testing
        [TestMethod]
        public void CheckAllPossibleHandCombinations()
        {
            BaseDeck shuffledDeck = new BaseDeck();
            BaseHand hand = new BaseHand(false);
            for (int i = 0; i < NUMBER_OF_CARDS_IN_CRIBBAGE_HAND_WITHOUT_CUT_CARD; i++)
            {
                hand.CardsInHand.Add(shuffledDeck.DrawCard());
            }

            //TODO: KEF provide logic to do cutCard which flips a card and add it to all hands
            BaseCard cutCard = shuffledDeck.DrawCard();
            cutCard.IsCutCard = true;
            hand.CardsInHand.Add(cutCard);

            //Values are invalid // multiple 15s, 2 runs of 3, 4 point flush, 1 double = 18 points verify and make test with this hand
            BaseCard four = new BaseCard() {IsCutCard = false, Points = 4, Type = 3, Suit = 1, Value = 15 };
            BaseCard five = new BaseCard() { IsCutCard = false, Points = 5, Type = 4, Suit = 1, Value = 20 };
            BaseCard six = new BaseCard() { IsCutCard = false, Points = 6, Type = 5, Suit = 1, Value = 25 };
            BaseCard fourCut = new BaseCard() { IsCutCard = true, Points = 4, Type = 3, Suit = 2, Value = 17 };
            BaseCard ten = new BaseCard() { IsCutCard = false, Points = 10, Type = 9, Suit = 1, Value = 31 };

            BaseHand testHand = new BaseHand(false);
            testHand.CardsInHand.Add(four);
            testHand.CardsInHand.Add(five);
            testHand.CardsInHand.Add(six);
            testHand.CardsInHand.Add(ten);
            testHand.CardsInHand.Add(fourCut);

            int pointsInTestHand = testHand.PointsInHand;
            Assert.IsTrue(pointsInTestHand == 18);

            int pointsInHand = -1;
            pointsInHand = hand.PointsInHand;
            Assert.IsTrue(pointsInHand >= 0);
        }
    }
}
