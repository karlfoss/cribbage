﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core;
using System;
using System.Collections.Generic;

namespace UnitTests
{
    /// <summary>
    /// TODO: should this service exist? Or should this functionality be moved into the hand class
    /// </summary>
    [TestClass]
    public class CoreUnitTests
    {
        private const int EXPECTED_NUMBER_OF_CRIBBAGE_HAND_COMBINATIONS = 15;
        private const int NUMBER_OF_CARDS_IN_CRIBBAGE_HAND = 4;
        //TODO: KEF make constants class to hold global constants like this
        private const int NUMBER_OF_CARDS_IN_CRIBBAGE_HAND_WITH_CUT_CARD = 5;
        private const int NUMBER_OF_CARDS_DEALT = 6;
        private const int BASE_TWO = 2;

        [TestMethod]
        public void CheckExpectedNumberOfHandCombinations()
        {
            List<bool[]> allHandCombinations = CalculatorService.GetAllPossibleCombinations(NUMBER_OF_CARDS_DEALT, NUMBER_OF_CARDS_IN_CRIBBAGE_HAND);
            Assert.IsTrue(allHandCombinations.Count == EXPECTED_NUMBER_OF_CRIBBAGE_HAND_COMBINATIONS);

            foreach (bool[] combination in allHandCombinations)
            {
                int count = 0;

                for (int i = 0; i < combination.Length; i++)
                {
                    if (combination[i])
                    {
                        count++;
                    }
                }

                Assert.IsTrue(count == NUMBER_OF_CARDS_IN_CRIBBAGE_HAND);
            }
        }

        /// <summary>
        /// This test should result in 2^Number_Of_Cards results because there either is or isn't a card in the combination.
        /// Thus resulting in a binary like counting that for the number of combinations
        /// Example 2^6 cards should result in 32 possible combinations (including no cards to compare). 
        /// </summary>
        /// TODO: KEF test this against constants of the expected results
        [TestMethod]
        public void CheckAllPossibleHandCombinations()
        {
            List<bool[]> allHandCombinations = CalculatorService.GetAllCombinations(NUMBER_OF_CARDS_IN_CRIBBAGE_HAND_WITH_CUT_CARD);
            Assert.IsTrue(allHandCombinations.Count == Math.Pow(BASE_TWO, NUMBER_OF_CARDS_IN_CRIBBAGE_HAND_WITH_CUT_CARD));
        }
    }
}
