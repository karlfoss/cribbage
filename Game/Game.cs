﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;

namespace Game
{
    public partial class Game : Form
    {
        /******* Game Constants (controlled by system)
         * .gitignore
         * Removed a bunch of compile files from source control tracking via following instructions:
         * https://stackoverflow.com/questions/11451535/gitignore-is-not-working
         * Still might want to actually remove them from the master branch altogether
         * 
         * 2D Game Image
         * https://www.google.com/imgres?imgurl=https%3A%2F%2Fs-media-cache-ak0.pinimg.com%2Foriginals%2F4e%2Fc8%2F3f%2F4ec83f3686b0877a08e72b279e6b6911.png&imgrefurl=https%3A%2F%2Fwww.pinterest.com%2Fpin%2F327355466654450148%2F&docid=Z31cHe5kKcD9xM&tbnid=Op0Gasw_Vo2nRM%3A&vet=10ahUKEwiuu92um8XbAhVSjK0KHdmMDyUQMwjpAygpMCk..i&w=2000&h=5991&safe=off&bih=748&biw=1463&q=cribbage%20board&ved=0ahUKEwiuu92um8XbAhVSjK0KHdmMDyUQMwjpAygpMCk&iact=mrc&uact=8
         * https://www.shutterstock.com/image-photo/high-quality-wood-cribbage-board-game-80927218
         *
         * 3D Game Image inspiration (basically 2d with better quality and some minor 2pm shadows
         * https://www.bleurghnow.com/img/bleurghnow/4-tracks-continuous-unique-cribbage-board-in-rosewood-handcrafted-inlaid-in-rosewood-maple-2295-x-843.jpg
         * Images can't overlap each other easily (workarounds below) very bad for moving parts though....
         * 
         * Enhancements:
         * TODO: Add logging to see what went wrong where / see what computer is doing and why and what it would do in my position
         * TODO: Use enum descriptions for Card for .ToString() (IE ACE instead of 0
         * TODO: Add cheatmode to see what the computer would select for discard (find ways to up ai)
         * TODO: Make crib selection significantly better. Find way to modify AI to learn with heuristics, and other AI methods. Give smaller weight to crib though
        * TODO: KEF it may be possible to not use pictureBoxes at all and to ONLY deal with the images
        *      This would likely be the optimum solution if they can move without stutter and overlap: http://www.vbforums.com/showthread.php?434581-Picture-Box-VS-Image-Box
        * TODO: KEF consider learning 2D unity because that option will be much easier
        * https://stackoverflow.com/questions/19910172/how-to-make-picturebox-transparent
        * https://stackoverflow.com/questions/5522337/c-sharp-picturebox-transparent-background-doesnt-seem-to-work
        * https://stackoverflow.com/questions/4623165/make-overlapping-picturebox-transparent-in-c-net
        */
        public Game()
        {
            InitializeComponent();
        }

        private void Game_Load(object sender, EventArgs e)
        {
            // Set the default screen
            //TODO: Auto fit logic, make enhancement to use this
            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            // Makes the game fullscreen
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BackColor = Color.ForestGreen;

            // TODO: Add a start menu and settings choices
            GameMenu();

            // TODO: check out Command Design pattern for invoking different games based on number of human/computesr http://www.dofactory.com/net/command-design-pattern
            // Load screen, reset all variables, and begin new game
            StartTwoPlayerGame();
        }

        private void GameMenu()
        {

        }

        /// <summary>
        /// TODO: SPLIT THIS OUT INTO SEPERATE CLASS in Game
        /// Start a 2 player game with 1 human and 1 AI
        /// </summary>
        private void StartTwoPlayerGame()
        {
            // Initialize all components
            Human human = new Human(this, Color.Blue);
            NormalComputer computer = new NormalComputer(this, Color.Red);

            //TODO: create a baseEntity that has a Game object (so everything can update itself??)
            //TODO: move almost all functionality into GameService, and have the gameService reference the eventService
            // Do rounds of gameplay until someone wins
            HumanVsComputerGame game = new HumanVsComputerGame(this, human, computer);
        }
    }
}
