﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Game
{
    /// <summary>
    /// ---> Why it doesn't make sense to implement an interface
    /// TODO: basically, an interface is a problem here because the core has no awareness
    /// of the entities I want to use. And any other implementation would probably use different entities
    /// anyway. Finally, the entire reason I moved entities into the Game project is because I need them
    /// to be able to add to the Controls (something they can't do outside of it due to circular dependencies)
    /// THIS GIVES TWO REMAINING SOLUTIONS
    ///     1: To get around this, I would need to move all controls back to the Game project and outside of the entites....which isn't a bad idea....
    ///         --> could work, but I need to decide if all of that could be handled GameService and Game
    ///     2: (leaning towards this) - just don't have an interface for GameService OR Implement the interface inside of Game
    /// https://stackoverflow.com/questions/31781696/define-interface-method-with-different-parameters-in-c-sharp
    /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/interfaces/
    /// </summary>
    public class TwoPlayerGame : BaseGame
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TwoPlayerGame(Game game): base(game)
        {

        }

        #endregion

        #region Events

        #endregion

        #region Private Methods

        #endregion
    }
}
