﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Game
{
    /// <summary>
    /// The base representation of all games for all common functionality and properties for any game instance
    /// 
    /// Relevant Notes
    /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// https://stackoverflow.com/questions/31781696/define-interface-method-with-different-parameters-in-c-sharp
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/interfaces/
    /// </summary>
    public abstract class BaseGame
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// The current game
        /// </summary>
        public Game Game
        {
            get; set;
        }

        /// <summary>
        /// The game board
        /// </summary>
        public Board Board
        {
            get; set;
        }

        /// <summary>
        /// The deck
        /// </summary>
        public Deck Deck
        {
            get; set;
        }

        /// <summary>
        /// The crib
        /// </summary>
        public Crib Crib
        {
            get; set;
        }

        /// <summary>
        /// The count for the pegging round
        /// </summary>
        public Count Count
        {
            get; set;
        }

        /// <summary>
        /// Number of cards currently selected by the human player
        /// </summary>
        public int NumberOfCardsSelected
        {
            get; set;
        }

        /// <summary>
        /// All cards used in the current round of pegging (resets at Go/31 points)
        /// index 0 is the first card played in the round up to n number of cards
        /// //TODO: consider having this hold all played cards and adding a boolean for if it is still being used in round?? seems like bad idea
        /// </summary>
        public List<Card> AllPlayedCardsInRound
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Default Constructor
        /// </summary>
        public BaseGame(Game game)
        {
            Game = game;
            Deck = new Deck();
            Board = new Board(Game);
            Crib = new Crib();
            Count = new Count();
        }

        /// <summary>
        /// TODO: move this to the baseGame
        /// </summary>
        /// <param name="lastCardPlayed"></param>
        public int CountPointsFromPlay(Card lastCardPlayed)
        {
            int numberOfPointsFromPlay = 0;
            int numberOfCardsMatchingInARow = 0;
            // Count the number of points from doubles (subtract by 2 because the first card is last card played)
            for (int i = AllPlayedCardsInRound.Count - 2; i >= 0; i--)
            {
                if (AllPlayedCardsInRound[i].Equals(lastCardPlayed))
                {
                    numberOfCardsMatchingInARow++;
                }
                else
                {
                    // Cards didn't match and the series (if any) is broken
                    break;
                }
            }

            if (numberOfCardsMatchingInARow == 1)
            {
                numberOfPointsFromPlay += 2;
            }
            else if (numberOfCardsMatchingInARow == 2)
            {
                numberOfPointsFromPlay += 6;
            }
            else if (numberOfCardsMatchingInARow == 3)
            {
                numberOfPointsFromPlay += 12;
            }

            return numberOfPointsFromPlay;
        }

        #endregion

        #region Events

        #endregion

        #region Private Methods

        #endregion
    }
}
