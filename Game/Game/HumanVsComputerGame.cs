﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Game
{
    /// <summary>
    /// A standard 2 player game that pits a computer against one of the ai instances. Follows all of the basic rules
    /// of cribbage
    /// 
    /// TODO: KEF minor enhancement - consider adding multi threading (ALL EVENTS WOULD NEED LOCKS)
    /// 
    /// Relevant Documentation:
    /// https://stackoverflow.com/questions/31781696/define-interface-method-with-different-parameters-in-c-sharp
    /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/interfaces/
    /// GameControl uses State design pattern: http://www.dofactory.com/net/state-design-pattern
    /// Event Guidelines: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/events/
    /// Finite State Machine: https://en.wikipedia.org/wiki/Finite-state_machine
    /// </summary>
    public class HumanVsComputerGame : TwoPlayerGame
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// Both of the players for the 2 player game
        /// NOTE: this is a base class declaration (it is abstract), so Player can not actually be used to initialize
        /// </summary>
        public List<Player> Players
        {
            get; set;
        }

        /// <summary>
        /// The human player in the 2 player game
        /// </summary>
        public Human HumanPlayer
        {
            get; set;
        }

        /// <summary>
        /// The computer player in the 2 player game
        /// NOTE: this is a base class declaration (it is abstract), so Computer can not actually be used to initialize
        /// </summary>
        public Computer ComputerPlayer
        {
            get; set;
        }

        /// <summary>
        /// Button for moving from game action to game action. These triggers are done by clicks
        /// from the human player
        /// Includes:
        ///     Deal - Deals cards to both players
        ///     Discard - Discard cards to crib
        /// </summary>
        public GameControl GameControl
        {
            get; set;
        }

        public Notification Notification
        {
            get; set;
        }

        public int CardSelectionLimit
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public HumanVsComputerGame(Game game, Human human, Computer computer): base(game)
        {
            // Set everything that will be needed for the game
            Game = game;
            HumanPlayer = human;
            ComputerPlayer = computer;
            Players = new List<Player>();
            Players.Add(HumanPlayer);
            Players.Add(ComputerPlayer);

            GameControl = new GameControl();
            GameControl.Display(Game);

            //TODO: make a class for this
            Notification = new Notification();

            //TODO: This is only used for debug and should be removed otherwise
            DebugLabel = new TextBox();
            DebugLabel.Width = 250;
            DebugLabel.Height = 50;
            DebugLabel.Font = new Font("Times New Roman", 28);
            DebugLabel.Text = "";
            DebugLabel.BackColor = Color.Orange;
            DebugLabel.Location = new Point(0 , 0);
            Game.Controls.Add(DebugLabel);

            Board.Display(Game);
            Board.PictureBox.Click += new EventHandler(ShowClickLocation_Click);

            // Start up the initial events that will be needed to allow the player to play   
            GameControl.Button.Click += CutForDeal_Click;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Stop and wait for the human player to select 2 cards to discard
        /// After this is called, 2 cards (from the original 6 in the deal) should
        /// have been selected
        /// </summary>
        private void HavePlayersChooseCardsForCrib()
        {
            GameControl.Button.Text = "Discard";

            // No cards could have been selected yet
            NumberOfCardsSelected = 0;

            // Make all cards selectable
            foreach (Card card in HumanPlayer.Hand.CardsInHand)
            {
                card.CardPicture.Click += new EventHandler(CardSelect_Click);
            }

            // Give the user a notification telling whose crib it is
            if (HumanPlayer.IsDealer)
            {
                Notification.Label.Text = "Select two cards to be discarded to your crib";
            }
            else
            {
                Notification.Label.Text = "Select two cards to be discarded to your opponent's crib";
            }

            GameControl.Button.Click += new EventHandler(DiscardForCrib_Click);

            // Choose cards from computer's hand for the crib
            ComputerPlayer.GetCardsToPutInCrib();
        }

        /// <summary>
        /// Sets up the first pegging round
        /// </summary>
        private void InitializePegging()
        {
            AllPlayedCardsInRound = new List<Card>();

            CardSelectionLimit = GeneralConstants.NUMBER_OF_CARDS_TO_PLAY_FOR_PEGGING;

            Count.Display(Game);

            // The non-dealer plays first
            if (HumanPlayer.IsDealer)
            {
                ComputerPlayer.IsTurn = true;
            }
            else
            {
                HumanPlayer.IsTurn = true;
            }

            InitializePeggingRound();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializePeggingRound()
        {
            // Reset the number of cards selected by the human
            NumberOfCardsSelected = 0;
            Count.Total = 0;
            Count.Label.Text = Count.Total.ToString();
            AllPlayedCardsInRound = new List<Card>();

            // Ensure no cards are selected for human before hand
            foreach (Card card in HumanPlayer.Hand.CardsInHand)
            {
                card.IsSelected = false;
            }

            // Play the round
            Play();
        }

        /// <summary>
        /// Play a single move (for the current player) in the pegging round
        /// </summary>
        private void Play()
        {
            if (Players.Where(a => a.IsTurn).Count() != 1)
            {
                // Only 1 player can play at a time for pegging
                throw new InvalidOperationException("Only one player can play for pegging at a time");
            }

            // It's the human's turn
            if (HumanPlayer.IsTurn)
            {
                //cardPlayed = HumanPlayer.Hand.CardsInHand.Single(a => a.IsSelected);
                Notification.Label.Text = "Select a card to play";
                // Make all cards selectable (that haven't already been played) and have user pick them
                foreach (Card card in HumanPlayer.Hand.CardsInHand)
                {
                    if (!card.IsPlayed)
                    {
                        card.CardPicture.Click += new EventHandler(CardSelect_Click);
                    }
                }
                // Set the next game state
                GameControl.Button.Click += PlayPeggingCard_Click;
            }
            // It's the computer's turn
            else
            {
                //TODO: continue here
                Card cardPlayed = ComputerPlayer.Play(Count.Total, ComputerPlayer.Hand);
                // Count the points (if any) and determine next move (if any)
                CheckPlay(cardPlayed);
            }
        }

        /// <summary>
        /// Check the result for the card that was chosen for the play
        /// </summary>
        private void CheckPlay(Card cardPlayed)
        {
            // Set card to played and add it to the played cards list
            cardPlayed.IsPlayed = true;

            // Add the points of the card played to the total count
            Count.Total += cardPlayed.Points;
            Count.Label.Text = Count.Total.ToString();

            // Add card to the list of currently played cards
            AllPlayedCardsInRound.Add(cardPlayed);

            // Count the points for the card and award them to the player that used it TODO: continue here
            CountPointsFromPlay(cardPlayed);
        }

        /// <summary>
        /// Count the cards in the players hand
        /// Returns true if the player won with their most recent hand
        /// </summary>
        /// <param name="player"></param>
        /// <param name="countCrib"></param>
        private bool CountPointsInHandOfPlayerAndCheckForWin(Player player)
        {
            int pointsInHand = 0;

            // Add cut card for counting points
            player.Hand.CardsInHand.Add(Deck.CutCard);
            // Show the hand if it isn't already being displayed
            pointsInHand = player.Hand.PointsInHand;
            // Add the points to the player's total
            player.TotalPoints += pointsInHand;

            // Check if the player one
            if (player.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN)
            {
                // The player won, so they can't score above 121
                player.TotalPoints = GeneralConstants.POINTS_NEEDED_TO_WIN;

                if (player.IsHuman)
                {
                    Notification.Label.Text = "You Won! You had " + pointsInHand + " in your final hand.";
                }
                else
                {
                    Notification.Label.Text = "The Computer Won! It had " + pointsInHand + " in its final hand.";
                }

                // Show final score (which should always be 121)
                player.TotalPointsLabel.Text = player.TotalPoints.ToString();

                // Move the peg to the winning position
                player.UpdateDisplayPegs();

                // Event for reseting the game
                GameControl.Button.Text = "New Game";
                GameControl.Button.Click += new EventHandler(NewGame_Click);
                return true;
            }
            else
            {
                // Only move a peg if the player gets more than 0 points in their hand
                if (pointsInHand > 0)
                {
                    player.UpdateDisplayPegs();
                }
                // Update the players score
                player.TotalPointsLabel.Text = player.TotalPoints.ToString();
            }

            // Display the points in the hand/crib
            if (player.IsHuman)
            {
                // Check if the hand or crib is being counted
                if (player.Hand.IsCrib)
                {
                    Notification.Label.Text = "You scored " + pointsInHand + " points in your crib";
                }
                else
                {
                    Notification.Label.Text = "You scored " + pointsInHand + " points in your hand";
                }
            }
            else
            {
                if (player.Hand.IsCrib)
                {
                    Notification.Label.Text = "The computer scored " + pointsInHand + " points in its crib";
                }
                else
                {
                    Notification.Label.Text = "The computer scored " + pointsInHand + " points in its hand";
                }
            }

            return false;
        }

        #endregion

        #region Events

        //TODO: for events, consider having a situation where we have multiple subscribers to an event. Thus allowing multiple cards to be discarded at once for example (IE have 2 cards subscribed to the Discard button to move upon a discard)

        /// <summary>
        /// Change the GameControl from Deal to Discard mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewGame_Click(object sender, EventArgs e)
        {
            // Don't allow more clicks until round is ready
            GameControl.Button.Click -= NewGame_Click;

            // The player that lost will be the next dealer
            Player winner = Players.Single(a => a.TotalPoints >= GeneralConstants.POINTS_NEEDED_TO_WIN);
            Player loser = Players.Single(a => a.TotalPoints < GeneralConstants.POINTS_NEEDED_TO_WIN);
            winner.IsDealer = false;
            loser.IsDealer = true;

            HumanPlayer.IsDealer = !HumanPlayer.IsDealer;
            ComputerPlayer.IsDealer = !ComputerPlayer.IsDealer;

            // TODO: The game should automatically deal if it is the PC's turn.
            // Change game button to deal if the human player is the new dealer (because they weren't last round)
            if (HumanPlayer.IsDealer)
            {
                GameControl.Button.Text = "Deal";
                Notification.Label.Text = "The computer won, so it is your deal";
            }
            else
            {
                GameControl.Button.Text = "Deal";
                Notification.Label.Text = "You won, so the computer will deal";
            }

            // Remove the hands of both players from display
            ComputerPlayer.ResetForNewGame(Game);
            HumanPlayer.ResetForNewGame(Game);

            Deck.RemoveDeckFromDisplay(Game);
            Crib.RemoveCribFromDisplay(Game);

            // Reset the deck and display it with the back of a Card
            Deck.CutCard = new Card() { IsCutCard = true };
            Deck.DisplayDeck(Game);
            // Reset the crib
            Crib = new Crib();

            // TODO: add dealing animation and ability for shuffle by human somehow?
            // TODO: have game go to deal event if human's turn, or selectCardsForCrib if computer's turn

            //Allows cards to be deselected after being selected
            GameControl.Button.Click += DealButton_Click;
        }

        /// <summary>
        /// Change the GameControl from Deal to Discard mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetRound_Click(object sender, EventArgs e)
        {
            // Don't allow more clicks until round is ready
            GameControl.Button.Click -= ResetRound_Click;

            // Remove the hands of both players from display
            ComputerPlayer.ResetForRound(Game);
            HumanPlayer.ResetForRound(Game);

            // Remove the deck from display and 
            Deck.ResetForRound(Game);

            // Reset the crib
            Crib = new Crib();

            // Switch the dealer to the player that didn't deal in the last round
            HumanPlayer.IsDealer = !HumanPlayer.IsDealer;
            ComputerPlayer.IsDealer = !ComputerPlayer.IsDealer;

            // TODO: The game should automatically deal if it is the PC's turn.
            if (HumanPlayer.IsDealer)
            {
                GameControl.Button.Text = "Deal";
                Notification.Label.Text = "Your turn to deal";
            }
            else
            {
                GameControl.Button.Text = "Deal";
                Notification.Label.Text = "Computer will deal";
            }

            // TODO: add dealing animation and ability for shuffle by human somehow?
            // TODO: have game go to deal event if human's turn, or selectCardsForCrib if computer's turn
            // OR at least have the user get a better message like "computer will deal", with a button text of "OK"

            //Allows cards to be deselected after being selected
            GameControl.Button.Click += DealButton_Click;
        }

        //TODO: Consider moving all Events to their own class/service?
        //TODO: Or divide the class by what they do
        //TODO: Main concern is how to reduce duplicate functionality....again...abstract class would be helpful

        /// <summary>
        /// Shows 52 cards (face down) for the player and computer to pick one to see who deals
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CutForDeal_Click(object sender, EventArgs e)
        {
            // Disable the cut for deal button
            GameControl.Button.Click -= CutForDeal_Click;

            // Ensure hands are cleared and not shown before cut
            HumanPlayer.RemoveHandFromDisplay(Game);
            ComputerPlayer.RemoveHandFromDisplay(Game);
            HumanPlayer.Hand = new Hand(false);
            ComputerPlayer.Hand = new Hand(false);

            // Hide the Notifications to show the cut cards
            Notification.RemoveFromDisplay(Game);

            Deck.NewDeck();
            Deck.Shuffle();
            Deck.DisplayAllCards(Game, CardClickedForCut_Click);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CardClickedForCut_Click(object sender, EventArgs e)
        {
            // Make all cards unclickable
            foreach (Card card in Deck.DeckOfCards)
            {
                card.CardPicture.Click -= CardClickedForCut_Click;
            }

            PictureBox cardSelectedPicture = (PictureBox)sender;

            // Draw the Card for the Human Player
            // SHOULD only be able to select a single card for an event
            Card cardSelectedHuman = Deck.DeckOfCards.Where(a => a.CardPicture == cardSelectedPicture).Single();
            Deck.RemoveCardFromDeck(cardSelectedHuman);
            HumanPlayer.Hand.CardsInHand.Add(cardSelectedHuman);
            HumanPlayer.DisplayInitialHand(Game, true);

            //TODO: add animations for computer and human selection

            Deck.Shuffle();
            Card cardSelectedComputer = Deck.DrawCard();
            ComputerPlayer.Hand.CardsInHand.Add(cardSelectedComputer);
            ComputerPlayer.DisplayInitialHand(Game, true);

            //Remove cards from display
            Deck.RemoveAllCardsFromDisplay(Game);
            Deck.DisplayDeck(Game);
            Notification.Display(Game);
            
            //TODO: should hands be hidden here, or in the deal/cut?
            if (cardSelectedHuman.Type < cardSelectedComputer.Type)
            {
                Notification.Label.Text = "You won the cut!";
                GameControl.Button.Text = "Deal";
                HumanPlayer.IsDealer = true;

                GameControl.Button.Click += DealButton_Click;
            }
            else if (cardSelectedHuman.Type > cardSelectedComputer.Type)
            {
                //TODO: Consider using multiple threads for this, see below
                //https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-based-asynchronous-programming
                //http://www.tutorialsteacher.com/csharp/csharp-anonymous-method
                Notification.Label.Text = "The computer won the cut, and will deal";
                GameControl.Button.Text = "OK";
                ComputerPlayer.IsDealer = true;

                GameControl.Button.Click += DealButton_Click;
            }
            else
            {
                // Players cut cards tied, have them try again
                Notification.Label.Text = "Your cards tied, try again";

                Deck.RemoveDeckFromDisplay(Game);

                // The cards are equal in type, have the players select the cards again
                GameControl.Button.Click += CutForDeal_Click;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DealButton_Click(object sender, EventArgs e)
        {
            // Change the GameControl from Deal to Discard mode
            GameControl.Button.Click -= DealButton_Click;

            // Ensure hands are cleared and not shown before deal
            HumanPlayer.RemoveHandFromDisplay(Game);
            ComputerPlayer.RemoveHandFromDisplay(Game);
            HumanPlayer.Hand = new Hand(false);
            ComputerPlayer.Hand = new Hand(false);

            // Put all cards back in the deck and shuffle
            Deck.NewDeck();
            Deck.Shuffle();

            // Deal six the cards to the players for a 2 player game
            for (int i = 0; i < GeneralConstants.NUMBER_OF_CARDS_DEALT; i++)
            {
                foreach (Player player in Players)
                {
                    player.Hand.CardsInHand.Add(Deck.DrawCard());
                }
            }

            // TODO eventually(when more advanced) add the dealing of card animation and show all cards face down at first
            
            // Display the cards for both players
            // Show the face value for the human player and hide the computer opponents' cards.
            HumanPlayer.DisplayInitialHand(Game, true);
            ComputerPlayer.DisplayInitialHand(Game, false);

            CardSelectionLimit = GeneralConstants.NUMBER_OF_CARDS_TO_DISCARD_FOR_CRIB;

            // Have human player selects 2 cards for the crib
            HavePlayersChooseCardsForCrib();
        }

        //TODO is locking of some sort needed for risk of multiple quick clicks?
        //How to do this in general?
        //Or is there no risk because this is single threaded (ie only 1 event can be fired at a time so this is autolocked until this method finishes)
        private void CardSelect_Click(object sender, EventArgs e)
        {
            // Don't allow selection if  other cards have been selected
            if (NumberOfCardsSelected < CardSelectionLimit)
            {
                NumberOfCardsSelected++;
                PictureBox cardSelectedPicture = (PictureBox)sender;
                cardSelectedPicture.Click -= CardSelect_Click;

                cardSelectedPicture.Location = new Point(cardSelectedPicture.Location.X, cardSelectedPicture.Location.Y - 50);

                // Mark the card selected
                // SHOULD only be able to select a single card for an event
                Card cardSelected = HumanPlayer.Hand.CardsInHand.Where(a => a.CardPicture == cardSelectedPicture).Single();
                cardSelected.IsSelected = true;

                //Allows cards to be deselected after being selected
                cardSelectedPicture.Click += CardDeselect_Click;
            }
        }

        //TODO is locking of some sort needed?
        private void CardDeselect_Click(object sender, EventArgs e)
        {
            NumberOfCardsSelected--;
            PictureBox cardDeSelectedPicture = (PictureBox)sender;
            cardDeSelectedPicture.Click -= CardDeselect_Click;

            // Mark the card as deselected
            Card cardSelected = HumanPlayer.Hand.CardsInHand.Where(a => a.CardPicture == cardDeSelectedPicture).Single();
            cardSelected.IsSelected = false;

            cardDeSelectedPicture.Location = new Point(cardDeSelectedPicture.Location.X, cardDeSelectedPicture.Location.Y + 50);

            // Allow cards to be selected after being deselected
            cardDeSelectedPicture.Click += CardSelect_Click;
        }

        //TODO is locking of some sort needed?
        /// <summary>
        /// Discards both the computer and human player's cards to the crib
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DiscardForCrib_Click(object sender, EventArgs e)
        {
            //TODO make constant
            //TODO consider how this would have to work if there were multiple human players.....would it work? if done one at a time? resetting each time?
            if (NumberOfCardsSelected == GeneralConstants.NUMBER_OF_CARDS_TO_DISCARD_FOR_CRIB)
            {
                // Disable the discard button
                GameControl.Button.Click -= DiscardForCrib_Click;

                // Disable all other click events
                foreach (Card card in HumanPlayer.Hand.CardsInHand)
                {
                    //TODO: find more modular and reusable way to do this
                    card.CardPicture.Click -= CardDeselect_Click;
                    card.CardPicture.Click -= CardSelect_Click;
                }

                Notification.Label.Text = "";

                //TODO: create method that removes cards from the players hands and puts them in the correct players crib
                // Get the cards selected for the crib
                List<Card> humanPlayerCardsForCrib = HumanPlayer.Hand.CardsInHand.Where(a => a.IsSelected == true).ToList();
                // Remove select cards for crib from human players hand
                foreach (Card card in humanPlayerCardsForCrib)
                {
                    card.RemoveCardFromDisplay(Game);
                }
                HumanPlayer.Hand.CardsInHand.RemoveAll(a => a.IsSelected == true);

                // Get the cards the AI selected for removal for the computer players
                List<Card> computerPlayerCardsForCrib = ComputerPlayer.Hand.CardsInHand.Where(a => a.IsSelected == true).ToList();
                // Remove the cards from display, remove them from the computer's hand, and put them in the crib
                foreach (Card card in computerPlayerCardsForCrib)
                {
                    card.RemoveCardFromDisplay(Game);
                }
                // Have computer player remove select/remove their cards for the crib (won't show this until human players chooses their 2 cards)
                ComputerPlayer.Hand.CardsInHand.RemoveAll(a => a.IsSelected == true);
                // Refresh the computers players hand

                // Add cards to the current dealer's crib
                Player dealer = Players.Single(a => a.IsDealer);
                Crib.CribHand.CardsInHand.AddRange(humanPlayerCardsForCrib);
                Crib.CribHand.CardsInHand.AddRange(computerPlayerCardsForCrib);
                // Show the crib
                Crib.DisplayCrib(Game, dealer);

                // Cut the card for the deck
                // TODO: Add animation here + selection for choosing cards for the crib
                // TODO: BUG - minor - this will continue to add deck to the controls, even if it isn't the first round
                Deck.CutCardForDeck(Game);

                //TODO: HAVE THIS CALL A NEW METHOD (IE CHECKFORWINNER) that is also called in the count hands section
                // Give the dealer 2 points if a jack comes up on the cut
                if (Deck.CutCard.Type == (int)Card.Types.Jack)
                {
                    dealer.TotalPoints += 2; //TODO: make constant for this
                    dealer.TotalPointsLabel.Text = dealer.TotalPoints.ToString();
                    dealer.UpdateDisplayPegs();
                }

                // Displays the hands after cards were removed for crib
                ComputerPlayer.DisplayHand(false);
                HumanPlayer.DisplayHand(true);

                // Wait for player to press OK to view the points from the non-dealer's hand
                GameControl.Button.Text = "OK";
                GameControl.Button.Click += new EventHandler(ShowPointsFromNonDealerHand_Click);
                //TODO: Continue work for implementing play round here
                //InitializePegging();
            }
            else
            {
                //TODO: add error message to tell users to select 2 cards for 5 seconds in the notifications bar or a 'warning' pop up...but have it allow 
                // other click events while the message is displaying
            }
        }

        /// <summary>
        /// Plays human card for the crib
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayPeggingCard_Click(object sender, EventArgs e)
        {
            //TODO consider how this would have to work if there were multiple human players.....would it work? if done one at a time? resetting each time?
            if (NumberOfCardsSelected == GeneralConstants.NUMBER_OF_CARDS_TO_PLAY_FOR_PEGGING)
            {
                // Disable the discard button
                GameControl.Button.Click -= PlayPeggingCard_Click;

                // Disable all other click events
                foreach (Card card in HumanPlayer.Hand.CardsInHand)
                {
                    //TODO: find more modular and reusable way to do this
                    card.CardPicture.Click -= CardDeselect_Click;
                    card.CardPicture.Click -= CardSelect_Click;
                }

                Notification.Label.Text = "";

                //TODO: create method that removes cards from the players hands and puts them in the correct players crib
                // Get the card selected to play for pegging
                Card cardPlayed = HumanPlayer.Hand.CardsInHand.Single(a => a.IsSelected == true);
                cardPlayed.IsPlayed = true;

                // Check the effect of the card played, update scores, and display the changes/notifications
                CheckPlay(cardPlayed);
            }
            else
            {
                //TODO: add error message to tell users to select 2 cards for 5 seconds in the notifications bar or a 'warning' pop up...but have it allow 
                // other click events while the message is displaying
            }
        }

        /// <summary>
        /// The selected card for the Human player gets pegged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCard_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Display the number of points the dealer scored and add them to the player's total score
        /// If the player reaches 121 for total points, the game ends
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowPointsFromNonDealerHand_Click(object sender, EventArgs e)
        {
            // Disable the OK button until points are displayed
            GameControl.Button.Click -= ShowPointsFromNonDealerHand_Click;

            // Get the points from the NonDealer's Hand
            Player nonDealer = Players.Single(a => !a.IsDealer);
            // Show cards if they aren't already being displayed
            nonDealer.DisplayHand(true);
            // Count points in the non-dealer's hand and check for win
            if (CountPointsInHandOfPlayerAndCheckForWin(nonDealer))
            {
                // Stop the event sequence if a player won
                return;
            }

            GameControl.Button.Click += ShowPointsFromDealerHand_Click;
        }

        /// <summary>
        /// Display the number of points the dealer scored and add them to the player's total score
        /// If the player reaches 121 for total points, the game ends
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowPointsFromDealerHand_Click(object sender, EventArgs e)
        {
            // Disable the OK button until points are displayed
            GameControl.Button.Click -= ShowPointsFromDealerHand_Click;

            // Get the points from the Dealer's Hand
            Player dealer = Players.Single(a => a.IsDealer);
            // Show cards if they aren't already being displayed
            dealer.DisplayHand(true);
            // Count points in the dealer's hand and check for win
            if (CountPointsInHandOfPlayerAndCheckForWin(dealer))
            {
                // Stop the event sequence if a player won
                return;
            }

            GameControl.Button.Click += ShowPointsFromCrib_Click;
        }

        /// <summary>
        /// Display the number of points the dealer scored and add them to the player's total score
        /// If the player reaches 121 for total points, the game ends
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowPointsFromCrib_Click(object sender, EventArgs e)
        {
            // Disable the OK button until points are displayed
            GameControl.Button.Click -= ShowPointsFromCrib_Click;

            // Get the points from the Dealer's Crib
            Player dealer = Players.Single(a => a.IsDealer);
            // Hide hand and show crib
            dealer.RemoveHandFromDisplay(Game);
            // Move the cards from the crib to the players hand and hide the crib
            dealer.Hand = Crib.CribHand;
            Crib.RemoveCribFromDisplay(Game);
            dealer.DisplayInitialHand(Game, true);
            // Counts the points in the crib and checks for win
            if (CountPointsInHandOfPlayerAndCheckForWin(dealer))
            {
                // Stop the event sequence if a player won
                return;
            }

            // Once 'OK' is clicked, the next round begins
            GameControl.Button.Click += ResetRound_Click;
        }

        #endregion

        #region Debug

        private TextBox DebugLabel
        {
            get; set;
        }

        /// <summary>
        /// Debugging program used to find the click position
        /// NOTE: THIS IS BASED ON THE SCREEN, not the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowClickLocation_Click(object sender, EventArgs e)
        {
            DebugLabel.Text = string.Format("X: {0} Y: {1}", Control.MousePosition.X, Control.MousePosition.Y);
        }

        #endregion
    }
}
