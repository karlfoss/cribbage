﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// Interface used for showing, removing, and updating objects for display
    /// </summary>
    public interface IDisplay
    {
        void Display(Game game);

        //TODO: how does it 'know' where to go?????
        // Need a service that knows the number of players, resolution, and resolves the position of the object knowing the game information
        // THEN when Update is called, it moves to to the assigned position
        void UpdateDisplay();

        void RemoveFromDisplay(Game game);
    }
}
