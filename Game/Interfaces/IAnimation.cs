﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// Relevant Documentation:
    /// How to draw images quickly: https://msdn.microsoft.com/en-us/library/system.drawing.graphics.drawimage(v=vs.110).aspx
    /// Otherwise, a clone picturebox could be used and moved in rapid succession
    /// 
    /// Interface used to define how objects look when moving. Used where objects locations are updated
    /// Examples: Dealing, Choosing cards for crib, pegging, etc
    /// </summary>
    public interface IAnimation
    {
        //TODO: NOTE to accomplish this timers will have to be set off to move each part indivdually before eventually triggering another event
        //TODO: if moving multiple parts at once, very careful drawing will have to be done
        //TODO: more complex methods may require parallelism
    }
}
