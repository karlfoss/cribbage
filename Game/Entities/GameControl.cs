﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// State based button that controls the game flow for the human player
    /// 
    /// Relevant Notes:
    /// GameControl uses State design pattern: http://www.dofactory.com/net/state-design-pattern
    /// </summary>
    public class GameControl : IDisplay
    {
        #region Constants

        #endregion

        #region Properties

        public Button Button
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public GameControl()
        {
            //TODO make constants for these
            //TODO make a class for this
            //Have a changeState method in it? That take a new event and text?
            Button = new Button();
            Button.Width = 250;
            Button.Height = 250;
            Button.Font = new Font("Times New Roman", 50);
            Button.Text = "Cut For Deal";
            Button.BackColor = Color.Purple;
        }

        public void Display(Game game)
        {
            Button.Location = new Point(game.Width - Button.Width, game.Height / 2 - Button.Height / 2);
            game.Controls.Add(Button);
        }

        public void UpdateDisplay()
        {
            throw new NotImplementedException();
        }

        public void RemoveFromDisplay(Game game)
        {
            game.Controls.Remove(Button);
        }

        #endregion
    }
}
