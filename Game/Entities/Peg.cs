﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    public class Peg : IDisplay
    {
        #region Constants

        #endregion

        #region Properties

        private int _point;

        // The display of the board
        public PictureBox PegPicture
        {
            get; set;
        }

        // Color of the player (IE the row the peg is in not actually the color of the peg)
        public Color Color
        {
            get; set;
        }

        // The point of the board the peg is on ((-2) to (121))
        public int Point
        {
            get { return _point; }
            // Used to offset the indexes. A player will only ever have 0 to 121 points.
            // However, we actually have 124 coordinates
            set
            {
                // Players points can't go above 121 (121 is a game win)
                if (value + 2 >= 123)
                {
                    _point = 123;
                }
                else
                {
                    _point = value + 2;
                }
            }
        }

        #endregion

        #region Public Methods

        public Peg(Color color, int point, Game game)
        {
            Point = point;
            Color = color;
            //TODO: this suffers similar problems as the Position of the players themselves
            // The peg should be independent of the position it should be in. IE blue peg should just know where to go given the point
            PegPicture = new PictureBox();
            // TODO: make this into a case switch
            // Use complementary colors for pegs or replace with images
            if (color.Equals(Color.Blue))
            {
                PegPicture.BackColor = Color.Orange;
            }
            else if (color.Equals(Color.Green))
            {
                PegPicture.BackColor = Color.Purple;
            }
            else if (color.Equals(Color.Red))
            {
                PegPicture.BackColor = Color.Yellow;
            }
            else
            {
                throw new InvalidOperationException(color.Name + " is not a valid color for a player");
            }
            PegPicture.Width = 10;
            PegPicture.Height = 10;
            Display(game);
        }

        public void Display(Game game)
        {
            int[,] coordinates;
            VisualConstants.PEG_COLOR_LOCATIONS.TryGetValue(Color.Name.ToString(), out coordinates);
            PegPicture.Location = new Point(coordinates[Point, 0] - PegPicture.Width / 2, coordinates[Point, 1] - PegPicture.Height / 2);
            game.Controls.Add(PegPicture);
        }

        public void UpdateDisplay()
        {
            int[,] coordinates;
            VisualConstants.PEG_COLOR_LOCATIONS.TryGetValue(Color.Name.ToString(), out coordinates);
            PegPicture.Location = new Point(coordinates[Point, 0] - PegPicture.Width / 2, coordinates[Point, 1] - PegPicture.Height / 2);
        }

        public void RemoveFromDisplay(Game game)
        {
            game.Controls.Remove(PegPicture);
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
