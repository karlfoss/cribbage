﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;


namespace Game
{
    public class Crib
    {
        #region Constants

        #endregion

        #region Properties

        public PictureBox CribPicture
        {
            get; set;
        }

        public Hand CribHand
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Crib()
        {
            CribHand = new Hand(true);
            CribPicture = new PictureBox();
            // Crib image is the back of a card by default
            CribPicture.Image = Image.FromFile(VisualConstants.BACK_OF_CARD_IMAGE_FILE);
            CribPicture.Width = Card.LARGE_CARD_WIDTH;
            CribPicture.Height = Card.LARGE_CARD_HEIGHT;
            CribPicture.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        //TODO: this should not be here. Crib should not care about 'player' for location. It needs a position class to remove coupling with player move to Game class instead?
        /// <summary>
        /// Displays the crib by the current dealer
        /// </summary>
        /// <param name="game">The current game</param>
        /// <param name="dealer">The dealer the crib goes by</param>
        public void DisplayCrib(Game game, Player dealer)
        {
            if (dealer.IsHuman)
            {
                // TODO: the 210 is to account for the score label, continue researching on ways to make this more dynamic? or at least have constants???
                CribPicture.Location = new Point(2560 - 210 - CribPicture.Width, 1440 - CribPicture.Height);
            }
            else
            {
                CribPicture.Location = new Point(2560 - 210 - CribPicture.Width , 0);
            }
            game.Controls.Add(CribPicture);
        }

        public void RemoveCribFromDisplay(Game game)
        {
            game.Controls.Remove(CribPicture);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
