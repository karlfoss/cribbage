﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    public class Count : IDisplay
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// TODO: consider giving a less generic name since this class could include other notifications + too generic
        /// </summary>
        public Label Label
        {
            get; set;
        }

        /// <summary>
        /// The total amount of points in the current pegging round
        /// </summary>
        public int Total
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Count()
        {
            // Initial Display settings
            Label = new Label();
            Label.Text = "0";
            Label.Width = 100;
            Label.Height = 100;
            Label.BackColor = Color.Yellow;
            Label.Font = new Font("Times New Roman", 30);

            // Set the initial point total
            Total = 0;
        }

        public void Display(Game game)
        {
            Label.Location = new Point(Deck.DECK_POSITION_X + (Card.LARGE_CARD_WIDTH - 100) / 2, Deck.DECK_POSITION_Y + Card.LARGE_CARD_HEIGHT + 10);
            game.Controls.Add(Label);
        }

        public void UpdateDisplay()
        {
            throw new NotImplementedException();
        }

        public void RemoveFromDisplay(Game game)
        {
            game.Controls.Remove(Label);
        }

        #endregion
    }
}
