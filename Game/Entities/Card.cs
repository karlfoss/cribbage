﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    public class Card
    {
        #region Constants

        // The card height
        public const int MEDIUM_CARD_HEIGHT = 200;
        public const int LARGE_CARD_HEIGHT = 300;
        // The card width
        //TODO: start using constants for this and make sure this matches the aspect ratio of the actual image
        public const int MEDIUM_CARD_WIDTH = 137; //MEDIUM_CARD_HEIGHT * 500 / 726;
        public const int LARGE_CARD_WIDTH = 207; // LARGE_CARD_HEIGHT * 500 / 726;

        // Card Types
        public enum Types
        {
            Ace,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King
        }

        // Possible Card Suits
        public enum Suits
        {
            Clubs,
            Diamonds,
            Hearts,
            Spades
        }

        #endregion

        #region Properties

        /// <summary>
        /// True if the cards should show its front
        /// False if it should show its back
        /// </summary>
        public bool ShowCard
        {
            get; set;
        }

        // The display of the card TODO: change to "Picture"
        public PictureBox CardPicture
        {
            get; set;
        }

        // Unique value of the card (IE 0 - 51)
        public int Value
        {
            get; set;
        }

        // The type of card (IE King, Queen, etc)
        public int Type
        {
            get; set;
        }

        // The suit of the card (Spades, Hearts, Diamonds, Clubs)
        public int Suit
        {
            get; set;
        }

        // The number of points the card is worth
        public int Points
        {
            get; set;
        }

        // True if this is the cut from the deck
        public bool IsCutCard
        {
            get; set;
        }

        // True if this card has already been used for played for pegging
        public bool IsPlayed
        {
            get; set;
        }

        /// <summary>
        /// True if this card was selected by a player
        /// Will be true if selected for crib or pegging
        /// </summary>
        public bool IsSelected
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Card()
        {
            Value = -1;
            Suit = -1;
            Type = -1;
            Points = -1;
            CardPicture = new PictureBox();
            // Default image to back of card
            //TODO: this is where another constant/service/constructor would be called if resolution or game size was different
            CardPicture.Image = Image.FromFile(VisualConstants.BACK_OF_CARD_IMAGE_FILE);
            CardPicture.Width = LARGE_CARD_WIDTH;
            CardPicture.Height = LARGE_CARD_HEIGHT;
            CardPicture.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        /// <summary>
        /// Displays the card at the specified location
        /// TODO: have all callers start using Constants (rather than relative positions) because relative just
        /// won't be flexible enough for different resolutions anyway
        /// </summary>
        /// <param name="game"></param>
        public void DisplayCard(Game game, int positionX, int positionY)
        {
            CardPicture.Location = new Point(positionX, positionY);
            game.Controls.Add(CardPicture);
        }

        public void RemoveCardFromDisplay(Game game)
        {
            game.Controls.Remove(CardPicture);
        }

        /// <summary>
        /// TODO: should I use new or override here?
        /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/knowing-when-to-use-override-and-new-keywords
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return String.Format(Enum.GetName(typeof(Types), Type) + " of " + Enum.GetName(typeof(Suits), Suit) + " - Value: " + Value + " Points: " + Points);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
