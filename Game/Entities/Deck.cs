﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// When to use 'override' or 'new' key words
    /// Summary: override for extending, and 'new' for hiding
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/knowing-when-to-use-override-and-new-keywords
    /// </summary>
    public class Deck
    {
        #region Constants

        //TODO: KEF have different X/Y  positions for different resolutions below we assume a resolution of 2560x1440
        //TODO maybe it would be worth having these somewhat dynamic, but I need a service to handle that logic instead so I don't have gross constructors
        public const int DECK_POSITION_X = Board.BOARD_WIDTH;  // used to be Game.Width / 20 - Card.LARGE_CARD_WIDTH / 2
        public const int DECK_POSITION_Y = 720 - Card.LARGE_CARD_HEIGHT / 2; // used to be  Game.Height / 2 - Deck.CutCard.CardPicture.Height / 2

        #endregion

        #region Properties

        //All of the cards in the deck
        public Stack<Card> DeckOfCards
        {
            get; set;
        }

        //Card cut from the deck after putting cards in the crib
        public Card CutCard
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Constructor for deck including the visual elements
        /// Loads 52 standard cards in a deck, and then shuffles them
        /// </summary>
        public Deck()
        {
            DeckOfCards = new Stack<Card>();
            NewDeck();

            // Show the back of the card as the default
            // TODO: No need to set IsCutCard here, when a card is actually cut this object will change
            CutCard = new Card() { IsCutCard = true };
        }

        /// <summary>
        /// Draws a card for the cut card
        /// TODO: have animation for this and when not dealing, have the human
        /// player choose the cut card
        /// </summary>
        /// <param name="game">The current game</param>
        public void CutCardForDeck(Game game)
        {
            RemoveDeckFromDisplay(game);
            CutCard = DrawCard();
            CutCard.IsCutCard = true;
            CutCard.CardPicture.Image = Image.FromFile(VisualConstants.IMAGES_FILE_PATH + CutCard.Value + VisualConstants.PNG_FILE_EXTENSION);
            DisplayDeck(game);
        }

        /// <summary>
        /// TODO: location of deck should not change, so these will never need to be set
        /// </summary>
        /// <param name="game"></param>
        public void DisplayDeck(Game game) //int positionX, int positionY)
        {
            CutCard.DisplayCard(game, DECK_POSITION_X, DECK_POSITION_Y);
        }

        /// <summary>
        /// Displays the entire Deck for cutting
        /// </summary>
        /// <param name="game"></param>
        public void DisplayAllCards(Game game, EventHandler CardClickedForCut_Click)
        {
            List<Card> cardsInDeck = DeckOfCards.ToList();
            int startingX = Board.BOARD_WIDTH + 10;
            int endX = 2560 - Card.LARGE_CARD_WIDTH - 250;// Reference that class when it is created GameControl.Width;
            int distanceXToCoverPerCard = (endX - startingX) / 51;
            int startingY = 1440 / 2 - Card.LARGE_CARD_HEIGHT / 2;

            for (int i = 51; i >= 0; i--)
            {
                cardsInDeck[i].CardPicture.Location = new Point(startingX + i * distanceXToCoverPerCard, startingY);
                cardsInDeck[i].CardPicture.Click += CardClickedForCut_Click;
                game.Controls.Add(cardsInDeck[i].CardPicture);
            }
        }

        /// <summary>
        /// Displays the entire Deck for cutting
        /// </summary>
        /// <param name="game"></param>
        public void RemoveAllCardsFromDisplay(Game game)
        {
            foreach (Card card in DeckOfCards)
            {
                game.Controls.Remove(card.CardPicture);
            }
        }

        public void RemoveDeckFromDisplay(Game game)
        {
            CutCard.RemoveCardFromDisplay(game);
        }

        /// <summary>
        /// Shuffles the cards by switching 2 cards at 'random' multiple times
        /// </summary>
        public void Shuffle()
        {
            Random rn = new Random();
            int card1;
            int card2;
            List<Card> shuffledDeckOfCards = DeckOfCards.ToList();

            for (int i = 0; i < GeneralConstants.NUMBER_OF_SHUFFLES; i++)
            {
                card1 = rn.Next(0, DeckOfCards.Count);
                card2 = rn.Next(0, DeckOfCards.Count);

                Card tempCard = shuffledDeckOfCards[card1];
                shuffledDeckOfCards[card1] = shuffledDeckOfCards[card2];
                shuffledDeckOfCards[card2] = tempCard;
            }

            // Put the cards back into the stack
            DeckOfCards.Clear();
            for (int i = 0; i < shuffledDeckOfCards.Count; i++)
            {
                DeckOfCards.Push(shuffledDeckOfCards[i]);
            }
        }

        /// <summary>
        /// Returns the top card from the deck(removing it from the deck in the process)
        /// </summary>
        /// <returns></returns>
        public Card DrawCard()
        {
            return DeckOfCards.Pop();
        }

        /// <summary>
        /// Creates a new deck with the basic 52 cards
        /// </summary>
        public void NewDeck()
        {
            // Burn the deck before loading a new one
            DeckOfCards.Clear();

            // Go through each suit
            for (int i = 0; i < GeneralConstants.NUMBER_OF_CARDS_PER_SUIT; i++)
            {
                // Go through all 13 cards per suit
                for (int j = 0; j < GeneralConstants.NUMBER_OF_SUITS; j++)
                {
                    Card newCard = new Card();

                    newCard.Suit = j;
                    newCard.Type = i;
                    newCard.Value = DeckOfCards.Count;

                    // Ten, Jack, Queen, and King all worth 10 points
                    if (i > GeneralConstants.LOWEST_CARD_TYPE_WORTH_TEN_POINTS)
                    {
                        newCard.Points = GeneralConstants.TEN_POINT_CARD;
                    }
                    else
                    {
                        //j (or CARD Type) is an index that starts at 0, but the lowest point value for a card is 1 (Ace)
                        newCard.Points = i + 1;
                    }

                    // Add the new card to the deck
                    DeckOfCards.Push(newCard);
                }
            }
        }

        /// <summary>
        /// Removes the passed card from the deck
        /// </summary>
        /// <param name="cardToRemove">Card to remove from the deck</param>
        public void RemoveCardFromDeck(Card cardToRemove)
        {
            List<Card> allCardsInDeck = DeckOfCards.ToList();

            // Remove the card from the list
            if (!allCardsInDeck.Remove(cardToRemove))
            {
                throw new InvalidOperationException("Could not remove " + cardToRemove + " from Deck");
            }

            // Clear out the current deck
            DeckOfCards.Clear();

            // Put the cards back in the deck in the order they were originally in (without the removed card)
            for (int i = allCardsInDeck.Count - 1; i >= 0; i--)
            {
                DeckOfCards.Push(allCardsInDeck[i]);
            }
        }

        /// <summary>
        /// Reset the deck for the new game after a player reaches 121 points
        /// </summary>
        public void ResetForNewGame()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reset the deck for the next round
        /// </summary>
        public void ResetForRound(Game game)
        {
            // Reset the deck and display it with the back of a Card
            RemoveDeckFromDisplay(game);
            // TODO: No need to set IsCutCard here, when a card is actually cut this object will change
            CutCard = new Card() { IsCutCard = true };
            DisplayDeck(game);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
