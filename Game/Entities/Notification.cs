﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// Class to display directions and information to the human player
    /// </summary>
    public class Notification : IDisplay
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// TODO: consider giving a less generic name since this class could include other notifications + too generic
        /// </summary>
        public Label Label
        {
            get; set;
        }

        #endregion

        #region Public Methods

        public Notification()
        {
            Label = new Label();
            Label.Width = 800;
            Label.Height = 100;
            Label.Font = new Font("Times New Roman", 30);
            Label.BackColor = Color.Transparent;
        }

        public void Display(Game game)
        {
            Label.Location = new Point(2560 / 2 - 400, 1440 / 2 - 50);
            game.Controls.Add(Label);
        }

        //NOTE: this is a failed experiment, basically, removing something form Controls affects
        //the label/picturebox/button BEFORE it finishes executing. Very weak workaround for the lack of 
        //async functions
        //public void DisplayMessageToUser(Game game, string text)
        //{
        //    game.Controls.Remove(Label);
        //    Label.Text = text;
        //    game.Controls.Add(Label);
        //}

        public void UpdateDisplay()
        {
            throw new NotImplementedException();
        }

        public void RemoveFromDisplay(Game game)
        {
            game.Controls.Remove(Label);
        }

        #endregion
    }
}
