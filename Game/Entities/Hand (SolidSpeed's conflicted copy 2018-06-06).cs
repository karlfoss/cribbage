﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Hand
    {
        #region Constants

        //NOTE: don't use static service to counts points. Utility classes are not worth it. Can't do testing as easily or polymorphism
        private const int VALID_HAND_SIZE = 5;
        private const int NUMBER_OF_CARDS_IN_PAIR = 2;
        private const int FIRST_CARD_IN_PAIR = 0;
        private const int SECOND_CARD_IN_PAIR = 1;

        private const int POINTS_FROM_PAIR = 2;
        private const int POINTS_FROM_KNOBS = 1;

        #endregion

        #region Properties

        public List<Card> CardsInHand
        {
            //TODO: KEF have this always return a sorted list (based on value)
            get; set;
        }

        /// <summary>
        /// The number of points in the hand
        /// </summary>
        public int PointsInHand
        {
            get { return CountPointsInHand(); }
        }

        /// <summary>
        /// True if this is the crib rather than the player's Hand
        /// OR rather, true if this wasn't the dealt hand
        /// The crib is the cards discarded by all players after being dealt
        /// </summary>
        public bool IsCrib
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Constructor for a Hand
        /// </summary>
        /// <param name="isCrib">True if this is the crib rather than the player's hand</param>
        public Hand(bool isCrib)
        {
            CardsInHand = new List<Card>();
            IsCrib = isCrib;
        }

        #endregion

        #region Private Methods

        private int CountPointsInHand()
        {
            int totalPointsInHand = 0;

            //TODO: There are only ever 4 cards in a cribbage hand, addBoolean for counting to consider counting Deck's card flipped up
            //TODO: alternatively, hack it, and just add the drawn card to both players hands
            if (CardsInHand.Count < 4 || CardsInHand.Count > 5)
            {
                //Invalid hand to count
                throw new InvalidOperationException("Must have 4 or 5 cards to count the points");
            }

            totalPointsInHand += CountAllFifteens();

            totalPointsInHand += CountAllRuns();

            totalPointsInHand += CountFlush();

            totalPointsInHand += CountPairs();

            totalPointsInHand += CountKnobs();

            return 0;
        }

        private int CountAllFifteens()
        {
            int pointsFromFifteens = 0;

            return pointsFromFifteens;
        }

        private int CountAllRuns()
        {
            int pointsFromRuns = 0;

            return pointsFromRuns;
        }

        private int CountFlush()
        {
            int pointsFromFlush = 0;
            int suit = -1;

            foreach (Card card in CardsInHand)
            {
                suit = card.Suit;
            }

            return pointsFromFlush;
        }

        /// <summary>
        /// Counts all of the points from pairs of cards
        /// 2 of a kind = 2 points
        /// 3 of a kind = 6 points (3 pairs)
        /// 4 of a kind = 12 points (4 pairs)
        /// </summary>
        /// <returns>The number of points from all pairs counted</returns>
        private int CountPairs()
        {
            int pointsFromPairs = 0;

            List<bool[]> allPairsToCheck = CalculatorService.GetAllPossibleCombinations(CardsInHand.Count, NUMBER_OF_CARDS_IN_PAIR);
            foreach (bool[] potentialPair in allPairsToCheck)
            {
                //CardsToCheck
                List<Card> potentialPairOfCards = CardsToCheckForPair(potentialPair, this.CardsInHand);
                if (potentialPairOfCards[FIRST_CARD_IN_PAIR].Type == potentialPairOfCards[SECOND_CARD_IN_PAIR].Type)
                {
                    pointsFromPairs += POINTS_FROM_PAIR;
                }
            }

            return pointsFromPairs;
        }

        /// <summary>
        /// Returns a single point if there is knobs or zero if there isn't
        /// </summary>
        /// <returns>The point gained if there is knobs</returns>
        private int CountKnobs()
        {
            int pointFromKnobs = 0;

            // Get the card that was flipped
            Card cutCard = CardsInHand.Where(a => a.IsCutCard == true).Single();

            // If the cut card isn't a Jack, then knobs isn't possible
            if (cutCard.Type != (int)Card.Types.Jack)
            {
                return pointFromKnobs;
            }

            // Check every Jack to see if it matches the suit of the flipped card
            foreach (Card card in CardsInHand)
            {
                // If the card isn't the cut one (IE identical), they are the same suit, and the card is a Jack
                // then knobs 
                if (card != cutCard && card.Suit == cutCard.Suit && card.Type == (int)Card.Types.Jack)
                {
                    // Can only get 1 point from Knobs, so return if met
                    pointFromKnobs += POINTS_FROM_KNOBS;
                    return pointFromKnobs;
                }
            }

            return pointFromKnobs;
        }

        /// <summary>
        /// Returns a list of cards
        /// </summary>
        /// <param name="cardsToGet"></param>
        /// <param name="listOfCards"></param>
        /// <returns></returns>
        private List<Card> CardsToCheckForPair(bool[] cardsToGet, List<Card> listOfCards)
        {
            List<Card> cardsMatched = new List<Card>();

            for (int i = 0; i < cardsToGet.Length; i++)
            {
                if (cardsToGet[i])
                {
                    cardsMatched.Add(listOfCards[i]);
                }
            }

            return cardsMatched;
        }

        #endregion
    }
}
