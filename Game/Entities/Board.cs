﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    public class Board : IDisplay
    {
        #region Constants

        // The board height
        public const int BOARD_HEIGHT = 1296; //used to be game.Height * 9 / 10;
        // The board width
        public const int BOARD_WIDTH = 432; //used to be BoardPicture.Height / 3;

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        // The display of the board
        /// <summary>
        /// The display of the board
        /// </summary>
        public PictureBox PictureBox
        {
            get; set;
        }

        /// <summary>
        /// Constructor for visual object + normal defaults
        /// </summary>
        public Board(Game game)
        {
            PictureBox = new PictureBox();
            PictureBox.Image = Image.FromFile(VisualConstants.BOARD_IMAGE_FILE);
            PictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

            //The Board Settings if it was Horizontal
            //BoardPicture.Width = game.Width * 4 / 5;
            //BoardPicture.Height = BoardPicture.Width / 3;
            //BoardPicture.Location = new System.Drawing.Point((game.Width / 10), game.Height / 2 - BoardPicture.Height / 2);

            //The Board Settings for Vertical View
            PictureBox.Height = BOARD_HEIGHT;
            PictureBox.Width = BOARD_WIDTH;
        }

        #endregion

        #region Private Methods

        public void Display(Game game)
        {
            PictureBox.Location = new System.Drawing.Point(0, game.Height / 2 - BOARD_HEIGHT / 2);
            game.Controls.Add(PictureBox);
        }

        public void UpdateDisplay()
        {
            throw new NotImplementedException();
        }

        public void RemoveFromDisplay(Game game)
        {
            game.Controls.Remove(PictureBox);
        }

        #endregion
    }
}
