﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Core;


namespace Game
{
    /// <summary>
    /// Represents a hand of the player
    /// A dealer will have a hand and a crib hand
    /// </summary>
    public class Hand
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// The cards in this hand
        /// </summary>
        public List<Card> CardsInHand
        {
            get; set;
        }

        /// <summary>
        /// The number of points in the hand
        /// </summary>
        public int PointsInHand
        {
            get { return CountPointsInHand(); }
        }

        /// <summary>
        /// True if this is the crib rather than the player's Hand, IE true if this wasn't the dealt hand
        /// The crib is the cards discarded by all players after being dealt
        /// </summary>
        public bool IsCrib
        {
            get; set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Constructor for a Hand
        /// </summary>
        /// <param name="isCrib">True if this is the crib rather than the player's hand</param>
        public Hand(bool isCrib)
        {
            CardsInHand = new List<Card>();
            IsCrib = isCrib;
        }

        /// <summary>
        /// Helper method that returns the cards to compare for a potential pair
        /// </summary>
        /// <param name="cardsToGet"></param>
        /// <returns></returns>
        public List<Card> RetrieveCardsFromCombination(bool[] cardsToGet)
        {
            if (cardsToGet.Length != CardsInHand.Count)
            {
                throw new InvalidOperationException("Can't retrieve cards because the list of cards doesn't line up with the requested cards");
            }

            List<Card> cardsMatched = new List<Card>();

            for (int i = 0; i < cardsToGet.Length; i++)
            {
                if (cardsToGet[i])
                {
                    cardsMatched.Add(CardsInHand[i]);
                }
            }

            return cardsMatched;
        }

        #endregion

        #region Private Methods

        private int CountPointsInHand()
        {
            int totalPointsInHand = 0;

            // The cut cards is added to both players hand, thus a valid hand is always 5
            //TODO: also check that there is a single cut card (we assume there must be one)
            if (CardsInHand.Count != GeneralConstants.VALID_HAND_SIZE)
            {
                //Invalid hand to count
                throw new InvalidOperationException("Must have 5 cards to be a valid hand");
            }

            totalPointsInHand += CountAllFifteens();

            totalPointsInHand += CountAllRuns();

            totalPointsInHand += CountFlush();

            totalPointsInHand += CountPairs();

            totalPointsInHand += CountKnobs();

            return totalPointsInHand;
        }

        /// <summary>
        /// Counts all of the points from fifteens
        /// Each combination of cards that add to fifteens results in 2 points
        /// </summary>
        /// <returns>The total points from fifteens</returns>
        private int CountAllFifteens()
        {
            int pointsFromFifteens = 0;

            // Check all combinations of cards
            // This will also check combinations of 0 and 1 because it will result in cleaner code that is actually probably more efficient
            // Just not fully logical because no card and a single card can't add to fifteen
            List<bool[]> allCombinationsToCheck = CalculatorService.GetAllCombinations(GeneralConstants.VALID_HAND_SIZE);

            foreach (bool[] combinationToCheck in allCombinationsToCheck)
            {
                int pointsFromCurrentCardCombination = 0;
                List<Card> cardsToCheckForFifteen = RetrieveCardsFromCombination(combinationToCheck);

                for (int i = 0; i < cardsToCheckForFifteen.Count; i++)
                {
                    pointsFromCurrentCardCombination += cardsToCheckForFifteen[i].Points;
                }

                if (pointsFromCurrentCardCombination == GeneralConstants.POINTS_NEEDED_FOR_FIFTEEN)
                {
                    pointsFromFifteens += GeneralConstants.POINTS_FROM_FIFTEEN;
                }
            }

            return pointsFromFifteens;
        }

        /// <summary>
        /// Count all of the points from runs
        /// Each run gives 1 point per card in the run
        /// 5 Card run = 5 points
        /// 4 Card run = 4 points
        /// 3 Card run = 3 points
        /// </summary>
        /// <returns>The number of points from all runs</returns>
        private int CountAllRuns()
        {
            int pointsFromRuns = 0;

            // Check for a run of five
            List<bool[]> combinationsToCheckForRunOfFive = CalculatorService.GetAllPossibleCombinations(CardsInHand.Count, GeneralConstants.NUMBER_OF_CARDS_IN_RUN_OF_FIVE);
            foreach (bool[] combinationToCheckForRunOfFive in combinationsToCheckForRunOfFive)
            {
                List<Card> cardsToCheckForRunOfFive = RetrieveCardsFromCombination(combinationToCheckForRunOfFive);

                if (IsRun(cardsToCheckForRunOfFive))
                {
                    pointsFromRuns += GeneralConstants.POINTS_FROM_RUN_OF_FIVE;
                }
            }

            // If a run of five has been found, then return because all other runs would be redundant
            if (pointsFromRuns > 0)
            {
                return pointsFromRuns;
            }

            // If no run of five, check for runs of 4
            List<bool[]> combinationsToCheckForRunOfFour = CalculatorService.GetAllPossibleCombinations(CardsInHand.Count, GeneralConstants.NUMBER_OF_CARDS_IN_RUN_OF_FOUR);
            foreach (bool[] combinationToCheckForRunOfFour in combinationsToCheckForRunOfFour)
            {
                List<Card> cardsToCheckForRunOfFour = RetrieveCardsFromCombination(combinationToCheckForRunOfFour);

                if (IsRun(cardsToCheckForRunOfFour))
                {
                    pointsFromRuns += GeneralConstants.POINTS_FROM_RUN_OF_FOUR;
                }
            }

            // If a run of four has been found, then return because all other runs would be redundant
            if (pointsFromRuns > 0)
            {
                return pointsFromRuns;
            }

            // If no runs of five/four, check for runs of 3
            List<bool[]> combinationsToCheckForRunOfThree = CalculatorService.GetAllPossibleCombinations(CardsInHand.Count, GeneralConstants.NUMBER_OF_CARDS_IN_RUN_OF_THREE);
            foreach (bool[] combinationToCheckForRunOfThree in combinationsToCheckForRunOfThree)
            {
                List<Card> cardsToCheckForRunOfThree = RetrieveCardsFromCombination(combinationToCheckForRunOfThree);

                if (IsRun(cardsToCheckForRunOfThree))
                {
                    pointsFromRuns += GeneralConstants.POINTS_FROM_RUN_OF_THREE;
                }
            }

            return pointsFromRuns;
        }

        /// <summary>
        /// Calculates the number of points from a flush if there is one
        /// Hand
        /// 4 original cards same suit = 4 points
        /// 4 original cards + cut card are the same suit = 5 points
        /// Crib
        /// 4 crib cards + cut card are the same suit = 5 points
        /// </summary>
        /// <returns>The number of points from a flush or zero if there isn't a flush</returns>
        private int CountFlush()
        {
            int pointsFromFlush = 0;
            // Grab the suit of a card that wasn't cut
            int suit = CardsInHand.Where(a => a.IsCutCard == false).First().Suit;

            // Get the original cards (non cut card) from the hand
            List<Card> originalCardsInHand = CardsInHand.Where(a => a.IsCutCard == false).ToList();

            // Make sure all of the cards in the original hand/crib are the same suit (required for a flush)
            foreach (Card card in originalCardsInHand)
            {
                //Technically does an extra check by checking the suit of the first card again (but this code is cleaner)
                if (card.Suit != suit)
                {
                    return pointsFromFlush;
                }
            }

            // Get the cut card
            // TODO: KEF consider making common queries class or making common query returns inside of this class as a private method
            Card cutCard = CardsInHand.Where(a => a.IsCutCard == true).Single();

            if (IsCrib == false)
            {
                // Flush has been met for hand, check for extra point from cut card
                if (cutCard.Suit == suit)
                {
                    pointsFromFlush += GeneralConstants.POINTS_FROM_FLUSH_WITH_MATCHING_CUT_CARD;
                }
                else
                {
                    pointsFromFlush += GeneralConstants.POINTS_FROM_FLUSH;
                }
            }
            else
            {
                //Crib requires the cut card match all of the other cards in the original crib
                if (cutCard.Suit == suit)
                {
                    pointsFromFlush += GeneralConstants.POINTS_FROM_FLUSH_WITH_MATCHING_CUT_CARD;
                }
            }

            return pointsFromFlush;
        }

        /// <summary>
        /// Counts all of the points from pairs of cards
        /// 2 of a kind = 2 points
        /// 3 of a kind = 6 points (3 pairs)
        /// 4 of a kind = 12 points (4 pairs)
        /// </summary>
        /// <returns>The number of points from all pairs counted</returns>
        private int CountPairs()
        {
            int pointsFromPairs = 0;

            // Gets all possible combinations of 2 cards to check for pairs
            List<bool[]> allPairsToCheck = CalculatorService.GetAllPossibleCombinations(CardsInHand.Count, GeneralConstants.NUMBER_OF_CARDS_IN_PAIR);
            foreach (bool[] potentialPair in allPairsToCheck)
            {
                // Check all unique sets of pairs of cards to see if they match
                List<Card> potentialPairOfCards = RetrieveCardsFromCombination(potentialPair);
                if (potentialPairOfCards[GeneralConstants.FIRST_CARD_IN_PAIR].Type == potentialPairOfCards[GeneralConstants.SECOND_CARD_IN_PAIR].Type)
                {
                    pointsFromPairs += GeneralConstants.POINTS_FROM_PAIR;
                }
            }

            return pointsFromPairs;
        }

        /// <summary>
        /// Returns a single point if there is knobs or zero if there isn't
        /// </summary>
        /// <returns>The point gained if there is knobs</returns>
        private int CountKnobs()
        {
            int pointFromKnobs = 0;

            // Get the card that was flipped
            // TODO: KEF consider making common queries class or making common query returns inside of this class as a private method
            Card cutCard = CardsInHand.Where(a => a.IsCutCard == true).Single();
            // Get every card that isn't the cut card TODO: Make query for this
            List<Card> cardsInOriginalHand = CardsInHand.Where(a => a.IsCutCard == false).ToList();

            // Check every Jack to see if it matches the suit of the flipped card
            foreach (Card card in cardsInOriginalHand)
            {
                // If the card isn't the cut one (IE identical), they are the same suit, and the card is a Jack
                // then knobs 
                if (card.Suit == cutCard.Suit && card.Type == (int)Card.Types.Jack)
                {
                    // Can only get 1 point from Knobs, so return if met
                    pointFromKnobs += GeneralConstants.POINTS_FROM_KNOBS;
                    return pointFromKnobs;
                }
            }

            return pointFromKnobs;
        }

        /// <summary>
        /// Sort the cards and check if there is a run
        /// </summary>
        /// <param name="cards">The cards to check for a run with</param>
        /// <returns>True if there is run, false otherwise</returns>
        private bool IsRun(List<Card> cards)
        {
            // TODO: KEF make a common query (probably private method in this class) for this
            // TODO: KEF note that this probably isn't as efficient as making an IComparable interface for Card, 
            // buuut hands are also only 5 length...sooo
            List<Card> sortedCards = cards.OrderBy(a => a.Value).ToList();

            // Don't check the last Card because the card before that already compared with it
            for (int i = 0; i < sortedCards.Count - 1 ; i++)
            {
                // Run is not possible if the second is not one type higher than the first card
                if (sortedCards[i].Type != sortedCards[i + 1].Type - 1)
                {
                    return false;
                }
            }

            // If all cards were within in 1 type of each other, then a run does exist
            return true;
        }

        #endregion
    }
}
