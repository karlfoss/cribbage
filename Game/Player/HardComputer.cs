﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// </summary>
    public class HardComputer : Computer
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public HardComputer(Game game, Color color): base(game, color)
        {
            //Calls the base constructor
        }

        public override List<Card> GetCardsToPutInCrib()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Selects and returns the card the computer chooses to peg
        /// TODO: eventually include all known cards so that the computer can have educated guesses
        /// </summary>
        /// <param name="count">The current count in the pegging round</param>
        /// <param name="hand">The computer player's hand</param>
        /// <returns>The card selected for playing for pegging</returns>
        public override Card Play(int count, Hand hand)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
