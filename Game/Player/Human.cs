﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// </summary>
    public class Human : Player
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public Human(Game game, Color color): base(game, color)
        {
            Hand = new Hand(false);
            TotalPoints = 0;
            IsHuman = true;

            // Display point boxes in separate locations for each player in 2 player game
            // TODO make visual constants for this
            TotalPointsLabel.Location = new Point(2360, 1240);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
