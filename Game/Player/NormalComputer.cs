﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// TODO: see virtual TIIIIIIIIISSSSSS -> https://www.tutorialspoint.com/csharp/csharp_polymorphism.htm
    /// </summary>
    public class NormalComputer : Computer
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public NormalComputer(Game game, Color color): base(game, color)
        {
            // Calls the base constructor
        }

        public override List<Card> GetCardsToPutInCrib()
        {
            Dictionary<List<Card>, double> allPossibleHandsWithProjectedPoints = new Dictionary<List<Card>, double>();
            Dictionary<List<Card>, int[]> allPossiblePointsTest = new Dictionary<List<Card>, int[]>();
            Deck deck = new Deck();
            //TODO: KEF consider overriding .equals method for cards
            //TODO: KEF gather other stats on other hands, the range of possible outcomes for the chosen hand, mode of best hand, mean of all possible hands and other??? Good stuff here
            List<Card> cardsNotDealt = deck.DeckOfCards.Where(a => Hand.CardsInHand.All(b => b.Value != a.Value)).ToList();

            List<bool[]> combinationsToCheckForBestHand = CalculatorService.GetAllPossibleCombinations(GeneralConstants.NUMBER_OF_CARDS_DEALT, GeneralConstants.NUMBER_OF_CARDS_KEPT);

            foreach (bool[] combinationToCheckForBestHand in combinationsToCheckForBestHand)
            {
                //Average points for the hand with all possible cards drawn
                double averagePointsForHand = 0;
                List<Card> possibleHand = Hand.RetrieveCardsFromCombination(combinationToCheckForBestHand);

                foreach (Card cardNotDealt in cardsNotDealt)
                {
                    Hand checkHand = new Hand(true);
                    foreach (Card cardInPossibleHand in possibleHand)
                    {
                        checkHand.CardsInHand.Add(cardInPossibleHand);
                    }
                    cardNotDealt.IsCutCard = true;
                    checkHand.CardsInHand.Add(cardNotDealt);
                    averagePointsForHand += checkHand.PointsInHand;
                }

                averagePointsForHand = averagePointsForHand / GeneralConstants.NUMBER_OF_CARDS_NOT_IN_HAND;
                allPossibleHandsWithProjectedPoints.Add(possibleHand, averagePointsForHand);
            }

            //TODO: check for ties, and then base it off of crib points somehow? Potentially give higher rating to dangerous/beneficial
            //TODO: consider using better judgment than the MaxAverage, MaxMedian might be better as it wouldn't get skewed for instance
            List<Card> bestHand = allPossibleHandsWithProjectedPoints.FirstOrDefault(a => a.Value == allPossibleHandsWithProjectedPoints.Values.Max()).Key;

            // Remove other 2 cards from the hand and add them to the crib
            List<Card> cardsDiscardedToCrib = Hand.CardsInHand.Where(a => bestHand.All(b => b.Value != a.Value)).ToList();

            // Mark cards chosen as selected
            foreach (Card card in cardsDiscardedToCrib)
            {
                card.IsSelected = true;
            }

            return cardsDiscardedToCrib;
        }

        /// <summary>
        /// Selects and returns the card the computer chooses to peg
        /// TODO: eventually include all known cards so that the computer can have educated guesses
        /// </summary>
        /// <param name="count">The current count in the pegging round</param>
        /// <param name="hand">The computer player's hand</param>
        /// <returns>The card selected for playing for pegging or null if it has no valid moves</returns>
        public override Card Play(int count, Hand hand)
        {
            List<Card> cardsNotPlayed = hand.CardsInHand.Where(a => !a.IsPlayed).ToList();
            // Must have card to play in order to be called
            if (cardsNotPlayed == null || !cardsNotPlayed.Any())
            {
                throw new InvalidOperationException("Must have card to play in order to play");
            }

            List<Card> validCardsToPlay = cardsNotPlayed.Where(a => a.Points + count <= GeneralConstants.COUNT_OF_31).ToList();

            // Return null if there are no valid plays
            if (validCardsToPlay == null || !validCardsToPlay.Any())
            {
                return null;
            }

            // Select a card randomly to play
            Random rn = new Random();
            int randomCard = rn.Next(validCardsToPlay.Count);
            Card cardToPlay = validCardsToPlay[randomCard];

            return cardToPlay;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
