﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// The base computer player which has all common ai/computer functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// 
    /// NOTE: constructor for base classes: https://stackoverflow.com/questions/3873343/can-i-inherit-constructors
    /// NOTE: uses strategy design pattern: http://www.dofactory.com/net/strategy-design-pattern
    ///     for implementing different algorithms for lower/higher difficulties
    /// NOTE: why use abstract https://stackoverflow.com/questions/2118055/how-to-force-overriding-a-method-in-a-descendant-without-having-an-abstract-bas
    /// </summary>
    public abstract class Computer : Player
    {
        #region Constants

        #endregion

        #region Properties

        #endregion

        #region Public Methods

        public Computer(Game game, Color color) : base(game, color)
        {
            Hand = new Hand(false);
            TotalPoints = 0;
            IsHuman = false;

            // Display point boxes in separate locations for each player in 2 player game
            Color = Color.Red;
            // TODO make visual constants for this
            // TODO these locations should be independent of the player and based on a position class/constant?
            TotalPointsLabel.Location = new Point(2360, 0);
        }

        public abstract List<Card> GetCardsToPutInCrib();

        /// <summary>
        /// Selects and returns the card the computer chooses to peg
        /// TODO: eventually include all known cards so that the computer can have educated guesses
        /// </summary>
        /// <param name="count">The current count in the pegging round</param>
        /// <param name="hand">The computer player's hand</param>
        /// <returns>The card selected for playing for pegging</returns>
        public abstract Card Play(int count, Hand hand);

        #endregion

        #region Private Methods

        #endregion
    }
}
