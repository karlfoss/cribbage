﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Core;

namespace Game
{
    /// <summary>
    /// The base player which has all common player functions and logic
    /// and specifically excludes non-shared functionality like Visual effects
    /// 
    /// NOTE: Abstract documentation: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/abstract
    /// </summary>
    public abstract class Player
    {
        #region Constants

        #endregion

        #region Properties

        /// <summary>
        /// The players current hand
        /// </summary>
        public Hand Hand
        {
            get; set;
        }

        /// <summary>
        /// The total points the player has, 121 needed to win
        /// </summary>
        public int TotalPoints
        {
            get; set;
        }

        /// <summary>
        /// True if the player is the dealer for the round
        /// </summary>
        public bool IsDealer
        {
            get; set;
        }

        /// <summary>
        /// True if the player is human, false if it is computer ai
        /// </summary>
        public bool IsHuman
        {
            get; set;
        }

        /// <summary>
        /// True if its the players turn to play in the pegging round
        /// </summary>
        public bool IsTurn
        {
            get; set;
        }

        /// <summary>
        /// Visual for pegs on the board 
        /// </summary>
        public List<Peg> Pegs
        {
            get; set;
        }

        /// <summary>
        /// Picture that shows the total points 
        /// </summary>
        public Label TotalPointsLabel
        {
            get; set;
        }

        /// <summary>
        /// The color of the player's pegs/points/etc
        /// TODO: allow players to choose colors
        /// </summary>
        public Color Color
        {
            get; set;
        }

        #endregion

        #region Public Methods

        //TODO: make a constructor for common classes
        public Player(Game game, Color color)
        {
            Color = color;

            // Each player has three pegs
            Pegs = new List<Peg>();
            Pegs.Add(new Peg(color, -2, game));
            Pegs.Add(new Peg(color, -1, game));
            Pegs.Add(new Peg(color, 0, game));

            // Display the TotalPointsLabel
            TotalPointsLabel = new Label();
            TotalPointsLabel.Text = "0";
            TotalPointsLabel.Font = new Font("Times New Roman", 50);
            TotalPointsLabel.Width = 200;
            TotalPointsLabel.Height = 200;
            TotalPointsLabel.BackColor = color;
            game.Controls.Add(TotalPointsLabel);
        }

        /// <summary>
        /// TODO: should this be here or in summary class
        /// 
        /// Display the cards for each player
        /// The humans' cards go on the bottom of the screen and are shown
        /// The computers' cards go on the top of the screen and are hidden
        /// Assumes two player game, additional methods would be needed for other game types
        /// </summary>
        /// <param name="game">The current game</param>
        /// <param name="showCards">True if the actual card values are shown, false if the card backs are shown</param>
        public void DisplayInitialHand(Game game, bool showCards)
        {
            //Reorder the list to be lowest to highest for display
            Hand.CardsInHand = Hand.CardsInHand.OrderBy(a => a.Value).ToList();
            if (IsHuman)
            {
                for (int i = 0; i < Hand.CardsInHand.Count; i++)
                {
                    //TODO: use foreach loop, have DisplayInitialCard() called instead to handle this (passes in x,y) positions....but for is need to count....oops
                    if (showCards)
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.IMAGES_FILE_PATH + Hand.CardsInHand[i].Value.ToString() + VisualConstants.PNG_FILE_EXTENSION);
                    }
                    else
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.BACK_OF_CARD_IMAGE_FILE);
                    }
                    Hand.CardsInHand[i].CardPicture.Location = new System.Drawing.Point(game.Width / 4 + Card.LARGE_CARD_WIDTH / 2 + Card.LARGE_CARD_WIDTH * i, game.Height - Card.LARGE_CARD_HEIGHT * 3 / 2);
                    game.Controls.Add(Hand.CardsInHand[i].CardPicture);
                }
            }
            else
            {
                //TODO: use foreach loop, have DisplayInitialCard() called instead to handle this (passes in x,y) positions....but for is need to count....oops
                for (int i = 0; i < Hand.CardsInHand.Count; i++)
                {
                    if (showCards)
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.IMAGES_FILE_PATH + Hand.CardsInHand[i].Value.ToString() + VisualConstants.PNG_FILE_EXTENSION);
                    }
                    else
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.BACK_OF_CARD_IMAGE_FILE);
                    }
                    Hand.CardsInHand[i].CardPicture.Location = new System.Drawing.Point(game.Width / 4 + Card.LARGE_CARD_WIDTH / 2 + Card.LARGE_CARD_WIDTH * i, Card.LARGE_CARD_HEIGHT * 2 / 3);
                    game.Controls.Add(Hand.CardsInHand[i].CardPicture);
                }
            }
        }

        /// <summary>
        /// Reset the player for the new game after a player reaches 121 points
        /// </summary>
        public void ResetForNewGame(Game game)
        {
            //Remove the hand for the display and clear previous hand
            RemoveHandFromDisplay(game);
            Hand = new Hand(false);

            //Reset Points
            TotalPointsLabel.Text = "0";
            TotalPoints = 0;

            //Reset Pegs
            for (int i = 0; i < Pegs.Count; i++)
            {
                //TODO: create constant for this offset
                Pegs[i].Point = i - 2;
                Pegs[i].UpdateDisplay();
            }
        }

        /// <summary>
        /// Reset the player for the next round
        /// </summary>
        public void ResetForRound(Game game)
        {
            // Remove the cards to 'put back in deck' and clear hand
            RemoveHandFromDisplay(game);
            Hand = new Hand(false);
        }

        /// <summary>
        /// TODO: shouldn't this actually be in the hand class
        ///     YES to above, this would make it independent of player
        /// TODO: does it make more sense to have these here or in the subclasses
        ///         NOOOO -> focus on making a position 1-4 that players can be assigned to
        /// Method that 'refreshes' the card pictures and locations
        /// Used after cards are dealt, and chosen for crib
        /// Computer players hands are hidden by default, and players
        /// 
        /// TODO: consider making separate entities for player/computer since they have different rules like below
        /// And then they can inherit directly from Player. This would work well for 2 player game at least, might be an issue
        /// at the higher level. Because right now it just does different behavior all in one class....which could make sense, have to think more
        /// </summary>
        /// <param name="showCards">True if the actual card values are shown, false if the card backs are shown</param>
        public void DisplayHand(bool showCards)
        {
            // TODO: be sure to not include cutCard in this, should be stopped here rather than in the GameService
            // Order the cards for display
            Hand.CardsInHand = Hand.CardsInHand.OrderBy(a => a.Value).ToList();
            if (IsHuman)
            {
                // Reorder the list to be lowest to highest for display
                for (int i = 0; i < Hand.CardsInHand.Count; i++)
                {
                    //TODO: KEF move this feature to dispalyInitialHand
                    if (showCards)
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.IMAGES_FILE_PATH + Hand.CardsInHand[i].Value.ToString() + VisualConstants.PNG_FILE_EXTENSION);
                    }
                    else
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.BACK_OF_CARD_IMAGE_FILE);
                    }
                    //TODO: use foreach loop, have DisplayCard() called instead to handle this (passes in x,y) positions
                    Hand.CardsInHand[i].CardPicture.Location = new System.Drawing.Point(2560 / 4 + Card.LARGE_CARD_WIDTH / 2 + Card.LARGE_CARD_WIDTH * i, 1440 - Card.LARGE_CARD_HEIGHT * 3 / 2);
                }
            }
            else
            {
                for (int i = 0; i < Hand.CardsInHand.Count; i++)
                {
                    // Hide 'opponents' card (which is computer player) until specified otherwise
                    if (showCards)
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.IMAGES_FILE_PATH + Hand.CardsInHand[i].Value.ToString() + VisualConstants.PNG_FILE_EXTENSION);
                    }
                    else
                    {
                        Hand.CardsInHand[i].CardPicture.Image = Image.FromFile(VisualConstants.BACK_OF_CARD_IMAGE_FILE);
                    }
                    //TODO: use foreach loop, have DisplayCard() called instead to handle this (passes in x,y) positions
                    Hand.CardsInHand[i].CardPicture.Location = new System.Drawing.Point(2560 / 4 + Card.LARGE_CARD_WIDTH / 2 + Card.LARGE_CARD_WIDTH * i, Card.LARGE_CARD_HEIGHT * 2 / 3);
                }
            }
        }

        /// <summary>
        /// TODO: make this into 1 method with boolean
        /// </summary>
        /// <param name="game"></param>
        public void RemoveHandFromDisplay(Game game)
        {
            foreach (Card card in Hand.CardsInHand)
            {
                // Don't remove cutCard because that is controlled by Deck
                if (!card.IsCutCard)
                {
                    card.RemoveCardFromDisplay(game);
                }
            }
        }

        public void UpdateDisplayPegs()
        {
            // Find the lowest score peg, and set it to the current score of the player
            Peg lowestPointPeg = Pegs.OrderBy(a => a.Point).First();
            lowestPointPeg.Point = TotalPoints;

            foreach (Peg peg in Pegs)
            {
                peg.UpdateDisplay();
            }
        }

        public void RemovePegsFromDisplay(Game game)
        {
            foreach (Peg peg in Pegs)
            {
                peg.RemoveFromDisplay(game);
            }
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
