Trello Board: https://trello.com/b/lkY8IdYR/cribbage  
Repo: https://karlfoss@bitbucket.org/karlfoss/cribbage.git  
Rules and background on Cribbage: https://en.wikipedia.org/wiki/Cribbage  

Cribbage is a card game that is typically played with two players. The objective is to score 121 points before your opponent in order to win.

Set one of the below projects as a startup project:  
**GameConsole Project:** This is fully implemented cribbage game currently set to simulate computer vs computer play. This was done in order to  
test different AI variants to see which one was generally strongest. In the future I plan to attempt a stronger AI with 'learning' capabilities. I also plan to add a  
command line interface to play human vs computer and to run simulations without having to modify code.

**Game Project:** This was my early attempt to get a visual portion of the game working. All logic and visual effects are there 
besides the 'play' round. You need your main screen set to, the admittedly odd resolution, 2560 x 1440 for it to work properly. It is currently  
set to play in a full screen mode. I have largely abandoned this project because of scaling issues, and because I wish to create this project  
on a stronger platform like Unity.